/**
 * Defines the format for an address
 */
package com.intheatersnow;

public class Address {
	private String street1;
	private String street2;
	private String city;
	private String state;
	private int zipCode;

	/**
	 * Sets the first line of the street
	 * @param street1
	 */
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	
	/**
	 * Gets the first line of the street
	 * @return first line of the street
	 */
	public String getStreet1() {
		return this.street1;
	}

	/**
	 * Sets the second line of the street
	 * @param street2
	 */
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	
	/**
	 * Gets the second line of the street
	 * @return 2nd line of street
	 */
	public String getStreet2() {
		return this.street2;
	}
	
	/**
	 * Gets the city
	 * @return city
	 */
	public String getCity() {
		return this.city;
	}
	
	/**
	 * Sets the city
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * Gets the state
	 * @return state
	 */
	public String getState() {
		return this.state;
	}
	
	/**
	 * Sets the state
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the zip code
	 * @return zip code
	 */
	public int getZipCode() {
		return this.zipCode;
	}
	
	/**
	 * Sets the zip code
	 * @param zipCode
	 */
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
}
