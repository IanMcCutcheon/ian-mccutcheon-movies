/**
 * Maps an internal exception to the corresponding HTTP status code.
 */
package com.intheatersnow.httpException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.intheatersnow.repositoryException.INTNException;

@Provider
@Component
public class INTNExceptionMapper implements ExceptionMapper<INTNException>{

	@Override
	public Response toResponse(INTNException ex) {
		switch (ex.getErrorCode()) {
			case INVALID_FIELD:
			case MISSING_DATA:
				return Response.status(Status.CONFLICT).entity(new HttpError(Status.CONFLICT, ex)).build();
				
			case NOT_FOUND:
				return Response.status(Status.NOT_FOUND).entity(new HttpError(Status.NOT_FOUND, ex)).build();
				
			case INTERNAL_ERROR:
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new HttpError(Status.INTERNAL_SERVER_ERROR, ex)).build();
				
			default:
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new HttpError(Status.INTERNAL_SERVER_ERROR, ex)).build();
		}
	}
}
