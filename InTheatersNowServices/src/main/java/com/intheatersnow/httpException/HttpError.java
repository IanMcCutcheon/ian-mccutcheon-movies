package com.intheatersnow.httpException;


import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.repositoryException.INTNException;


@XmlRootElement(name = "error")
public class HttpError {
	@XmlElement
	private int status;
	
	@XmlElement
	private String code;
	
	@XmlElement
	private String message;
	
	@XmlElement
	private String debug;

	protected HttpError(){}
	
	public HttpError(Response.Status conflict, INTNException ex) {
		this.status=conflict.getStatusCode();
		this.code=ex.getErrorCode()==null?"":ex.getErrorCode().name();
		this.message=ex.getMessage();
		this.debug=ex.getCause()==null?"":"caused by"+ex.getCause().getMessage();		
	}
}
