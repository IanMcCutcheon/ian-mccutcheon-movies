/**
 * Defines the format for the purchase request as sent over HTTP
 */
package com.intheatersnow.httpEntity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.Entity.TicketType;

@XmlRootElement(name = "purchaseRequest")
public class httpPurchaseRequest {
	@XmlElement
	public String accountNumber;
		
	@XmlElement
	public long showtimeId;
	
	@XmlElement
	public List<TicketType>tickets;
}
