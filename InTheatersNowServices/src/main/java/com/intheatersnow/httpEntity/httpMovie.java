/**
 * Defines the format for the movie as sent over HTTP
 */

package com.intheatersnow.httpEntity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.Entity.GenreName;
import com.intheatersnow.Entity.IMovie;

@XmlRootElement(name = "movie")
public class httpMovie {
	@XmlElement
	public long id;
	@XmlElement		
	public String title;
	@XmlElement		
	public String synopsis;
	@XmlElement		
	public Calendar releaseDate;
	@XmlElement
	public String ratingExplaination;
	@XmlElement		
	public long runningTime;
	@XmlElement	
	public String rating;
	@XmlElement
	public List<String>genres;
		
	//required by framework
	protected httpMovie() {}

	public httpMovie(IMovie movie) {
		Set<GenreName>movieGenres;
		
		this.id = movie.getId();
		this.title = movie.getTitle();
		this.synopsis = movie.getSynopsis();
		this.releaseDate = movie.getReleaseDate();
		this.runningTime = movie.getRunningTime();
		this.ratingExplaination = movie.getRatingExplanation();
		this.rating = movie.getRating();
        this.genres = new ArrayList<String>();
        
        movieGenres = movie.getGenres();
        if (movieGenres != null) {
        	for (GenreName genre:movieGenres) {
        		this.genres.add(genre.getGenre());
        	}
        }
	}
}
