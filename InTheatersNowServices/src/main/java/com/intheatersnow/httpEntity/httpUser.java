/**
 * Defines the format for the user as sent over HTTP
 */
package com.intheatersnow.httpEntity;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.Address;
import com.intheatersnow.Entity.IUser;

@XmlRootElement(name = "user")
public class httpUser {
	@XmlElement
	public long id;
	
	@XmlElement
	public String account;

	@XmlElement
	public String name;
	
	@XmlElement
	public String street1;
	
	@XmlElement
	public String street2;
	
	@XmlElement
	public String city;
	
	@XmlElement
	public String state;
	
	@XmlElement
	public int zipCode;
	
	@XmlElement
	public Calendar whenJoined;
	
	// Don't report the credit card information but 
	// do indicate if it is valid or not.
	@XmlElement
	public boolean creditCardValid;
	
	//required by framework
	protected httpUser() {}

	public httpUser(IUser user) {
		Address address;
		
		this.id = user.getId();
		this.account = user.getAccount();
		this.name = user.getName();
		this.whenJoined = user.getDateJoined();
		
		address = user.getAddress();
		if (address != null) {
			this.street1 = address.getStreet1();
			this.street2 = address.getStreet2();
			this.city = address.getCity();
			this.state = address.getState();
			this.zipCode = address.getZipCode();
		}
		
		this.creditCardValid = user.isCreditCardValid();
	}
}
