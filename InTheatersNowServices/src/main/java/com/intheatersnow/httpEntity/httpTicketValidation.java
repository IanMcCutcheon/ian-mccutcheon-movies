package com.intheatersnow.httpEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ticketValidation")
public class httpTicketValidation {
	@XmlElement
	public String barcode;
		
	@XmlElement
	public boolean valid;
		
	@XmlElement
	public String notValidReason;
		
	//required by framework
	protected httpTicketValidation() {}
}
