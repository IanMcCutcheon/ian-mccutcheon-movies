/**
 * Defines the structure of a theater as sent over HTTP
 */
package com.intheatersnow.httpEntity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.Address;
import com.intheatersnow.Entity.IMovieScreen;
import com.intheatersnow.Entity.ITheater;

@XmlRootElement(name = "theater")
public class httpTheater {
	@XmlElement
	public long id;
	@XmlElement
	public String name;
	@XmlElement
	public String phoneNumber;
	@XmlElement
	public String street1;
	@XmlElement
	public String street2;
	@XmlElement
	public String city;
	@XmlElement
	public String state;
	@XmlElement
	public int zipCode;
	@XmlElement
	public List<httpMovieScreen> screens;
	
	//required by framework
	protected httpTheater() {}

	public httpTheater(ITheater theater) {
		Address address;
		
		this.id = theater.getId();
		this.name = theater.getName();
		this.phoneNumber = theater.getPhoneNumber();
		
		address = theater.getAddress();
		this.street1 = address.getStreet1();
		this.street2 = address.getStreet2();
		this.city = address.getCity();
		this.state = address.getState();
		this.zipCode = address.getZipCode();

		List<IMovieScreen>screens = theater.getScreens();
		this.screens = new ArrayList<httpMovieScreen>(screens.size());
		for (IMovieScreen screen:screens) {
        	this.screens.add(new httpMovieScreen(screen));
        }
	}
}
