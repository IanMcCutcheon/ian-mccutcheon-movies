package com.intheatersnow.httpEntity;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
public class httpUserRequest {
	@XmlElement
	public long id;
		
	@XmlElement
	public String account;

	@XmlElement
	public String name;
		
	@XmlElement
	public String street1;
		
	@XmlElement
	public String street2;
		
	@XmlElement
	public String city;
		
	@XmlElement
	public String state;
		
	@XmlElement
	public int zipCode;
		
	@XmlElement
	public Calendar whenJoined;
	
	@XmlElement
	public String creditCardNumber;
	
	@XmlElement
	public int creditCardSecurityCode;
	
	@XmlElement
	public Calendar creditCardExpiration;
	
	//required by framework
	protected httpUserRequest() {}
}
