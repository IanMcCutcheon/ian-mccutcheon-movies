package com.intheatersnow.httpEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ticket")
public class httpTicket {
	@XmlElement
	public float price;
	
	@XmlElement
	public String type;
}
