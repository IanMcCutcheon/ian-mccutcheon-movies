package com.intheatersnow.httpEntity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.Entity.IShowtime;
import com.intheatersnow.Entity.ITicket;

@XmlRootElement(name = "showtime")
public class httpShowtime {
	@XmlElement
	public long showtimeId;
	
	@XmlElement
	public long theaterId;
	
	@XmlElement
	public long movieId;
	
	@XmlElement
	public long screenId;

	@XmlElement
	public Calendar showing;
	
	@XmlElement
	public List<httpTicket> tickets;
	
	@XmlElement
	public boolean ticketsAvailable;
	
	//required by framework
	protected httpShowtime() {}

	public httpShowtime(IShowtime showtime, int availTickets) {
		List<ITicket>tickets;
		httpTicket hTicket;
		
		this.showtimeId = showtime.getShowtimeId();
		this.theaterId = showtime.getTheaterId();
		this.movieId = showtime.getMovieId();
		this.screenId = showtime.getScreenId();
		this.showing = showtime.getShowtime();
		this.ticketsAvailable = availTickets > 0;
		
        tickets = showtime.getTickets();
        this.tickets = new ArrayList<httpTicket>();
        for (ITicket ticket:tickets) {
        	hTicket = new httpTicket();
        	hTicket.price = ticket.getPrice();
        	hTicket.type = ticket.getTicketType().getType();
        	this.tickets.add(hTicket);
        }
	}
}
