/**
 * Defines the structure of a movie screen as sent over HTTP
 */
package com.intheatersnow.httpEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.Entity.IMovieScreen;

@XmlRootElement(name = "movieScreen")
public class httpMovieScreen {
	@XmlElement
	public long id;
	
	@XmlElement
	public String name;
	
	@XmlElement
	public String seatType;
	
	@XmlElement
	public int numberSeats;
	
	@XmlElement
	public String screenType;
	
	@XmlElement
	public boolean accessibilityDevicePresent;
	
	//required by framework
	protected httpMovieScreen() {}

	public httpMovieScreen(IMovieScreen screen) {
		this.id = screen.getId();
		this.name = screen.getName();
		this.seatType = screen.getSeatType();
		this.screenType = screen.getScreenType();
		this.numberSeats = screen.getNumberSeats();
		this.accessibilityDevicePresent = screen.getAccessibilityDeviceAvailable();
	}
}
