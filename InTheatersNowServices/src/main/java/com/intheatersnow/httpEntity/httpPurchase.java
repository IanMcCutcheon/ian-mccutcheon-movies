/**
 * Defines the format for the purchase as sent over HTTP
 */

package com.intheatersnow.httpEntity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "purchase")
public class httpPurchase {
	@XmlElement
	public String accountNumber;
			
	@XmlElement
	public long showtimeId;
		
	@XmlElement
	public List<String> barcodes;
}
