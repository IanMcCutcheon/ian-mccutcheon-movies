/**
 * Spring configuration file which tells Spring which package to scan and 
 * forces the scan for components.
 */
package com.intheatersnow.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//@Configuration
//@ComponentScan(basePackages="com.intheatersnow")
//public class InTheatersNowConfig {
//
//}
