package com.intheatersnow.Entity;

import java.util.List;

import com.intheatersnow.Address;

public interface ITheater {
	public long getId();
	public String getName();
	public Address getAddress();
	public String getPhoneNumber();
	public int getNumberScreens();
	public List<IMovieScreen> getScreens();
	public void setId(long id);
	public void setName(String name);
	public void setStreet1(String street1);
	public void setStreet2(String street2);
	public void setCity(String city);
	public void setState(String state);
	public void setZipCode(int zipCode);
	public void setPhoneNumber(String phoneNumber);
	public void addScreen(IMovieScreen movieScreen);
	public IMovieScreen getScreenById(long screenId);
}
