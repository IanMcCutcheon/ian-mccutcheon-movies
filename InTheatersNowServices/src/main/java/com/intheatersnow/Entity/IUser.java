package com.intheatersnow.Entity;

import java.util.Calendar;

import com.intheatersnow.Address;
import com.intheatersnow.CreditCard2;

public interface IUser {
	public String getAccount();
	public String getName();
	public Address getAddress();
	public Calendar getDateJoined();
	public CreditCard2 getCreditCard();
	public void setId(long id);
	public void setAccount(String account);
	public void setName(String name);
	public void setStreet1(String street1);
	public void setStreet2(String street2);
	public void setCity(String city);
	public void setState(String state);
	public void setZipCode(int zipCode);
	public void setDateJoined(Calendar dateJoined);
	public void setCreditCardNumber(String creditCardNumber);
	public void setCreditCardSecurityCode(int securityCode);
	public void setCreditCardExpiration(Calendar creditCardExpiration);
	public long getId();
	public boolean isCreditCardValid();
}
