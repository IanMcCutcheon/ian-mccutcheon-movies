/**
 * MovieGenre.java
 * 
 * List of Film Genres, from IMDb (www.imdb.com/genre)
 *
 */

package com.intheatersnow.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GenreName")
public class GenreName {
	@Id
	@Column(name="GenreString")
	private String genre;
	
	public GenreName() { 
	}
	
	public GenreName(String genre) { 
		this.genre = genre;
	}
	
	public String getGenre() {
		return this.genre;
	}
	
	public void setGenre(String genre) {
		this.genre  = genre;
	}
}
