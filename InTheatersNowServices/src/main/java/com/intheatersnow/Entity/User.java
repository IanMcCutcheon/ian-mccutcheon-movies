/**
 * Defines a user within the system. Users need to be members in order
 * to make a purchase as purchases are recorded against their account
 * number.
 */

package com.intheatersnow.Entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.intheatersnow.Address;
import com.intheatersnow.CreditCard2;

@Entity
@Table(name="User")
public class User implements IUser {
	@Id
	@Column(name="idUser")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="Account")
	private String account;

	@Column(name="Name")
	private String name;
	
	@Column(name="Street1")
	private String street1;
	
	@Column(name="Street2")
	private String street2;
	
	@Column(name="City")
	private String city;
	
	@Column(name="State")
	private String state;
	
	@Column(name="zipCode")
	private int zipCode;
	
	@Column(name="whenJoined")
	private Calendar whenJoined;
	
	@Column(name="creditCardNumber")
	private String creditCardNumber;
	
	@Column(name="creditCardSecurityCode")
	private int creditCardSecurityCode;
	
	@Column(name="creditCardExpiration")
	private Calendar creditCardExpiration;
	
	/**
	 * Returns the user's account number
	 */
	public String getAccount() {
		return this.account;
	}
	
	/**
	 *  Returns the user's name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the user's address
	 */
	public Address getAddress() {
		Address address = new Address();
		address.setStreet1(this.street1);
		address.setStreet2(this.street2);
		address.setCity(this.city);
		address.setState(this.state);
		address.setZipCode(this.zipCode);
		
		return address;
	}

	public Calendar getDateJoined() {
		return this.whenJoined;
	}

	public CreditCard2 getCreditCard() {
		CreditCard2 creditCard = new CreditCard2();
		if (this.creditCardExpiration != null) { 
			creditCard.setExpiration(this.creditCardExpiration);
		}
		
		if (this.creditCardNumber != null) {
			creditCard.setNumber(this.creditCardNumber);
		}
		
		creditCard.setSecurityCode(this.creditCardSecurityCode);
		return creditCard;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setAccount(String account) {
		this.account = account;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sets the street1 for the user
	 */
	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	/**
	 * Sets the street2 for the user
	 */
	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	/**
	 * Sets the city for the user
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Sets the zip code for the user
	 */
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Sets the state for the user
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Sets the date when the user joined InTheatersNow
	 */
	public void setDateJoined(Calendar dateJoined) {
		this.whenJoined = dateJoined;
	}

	/**
	 * Sets the number of the credit card
	 */
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	/**
	 * Sets the security code of the credit card
	 */
	public void setCreditCardSecurityCode(int securityCode) {
		this.creditCardSecurityCode = securityCode;
	}

	/**
	 * Sets the expiration date of the credit card
	 */
	public void setCreditCardExpiration(Calendar creditCardExpiration) {
		this.creditCardExpiration = creditCardExpiration;
	}

	/**
	 * Gets the user's unique id
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * Checks the credit card to see if it is valid.  Given this exercise all we can 
	 * check is that there is a registered credit card and the expiration date is after
	 * today.
	 */
	public boolean isCreditCardValid() {
		if (this.creditCardNumber != null && 
				this.creditCardSecurityCode != 0 &&
				this.creditCardExpiration != null) {
			if (this.creditCardExpiration.after(Calendar.getInstance())) {
				return true;
			}
		}
		
		return false;
	}
}
