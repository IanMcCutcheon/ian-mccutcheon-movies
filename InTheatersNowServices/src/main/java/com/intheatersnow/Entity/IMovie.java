package com.intheatersnow.Entity;

import java.util.Calendar;
import java.util.Set;

public interface IMovie {
	public long getId();
	public String getTitle();
	public Calendar getReleaseDate();
	public String getRating();
	public String getRatingExplanation();
	public String getSynopsis();
	public long getRunningTime();
	public Set<GenreName>getGenres();
	
	public void setId(long id);
	public void setTitle(String title);
	public void setReleaseDate(Calendar releaseDate);
	public void setRating(MovieRating rating);
	public void setRatingExplanation(String ratingExplanation);
	public void setSynopsis(String synopsis);
	public void setRunningTime(long runningTime);
	public void addGenre(GenreName genre);
}
