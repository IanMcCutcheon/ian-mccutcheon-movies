package com.intheatersnow.Entity;

public interface ITicket {
	public long getId();
	public void setId(long id);
	public float getPrice();
	public void setPrice(float price);
	public TicketType getTicketType();
	public void setTicketType(TicketType type);
}
