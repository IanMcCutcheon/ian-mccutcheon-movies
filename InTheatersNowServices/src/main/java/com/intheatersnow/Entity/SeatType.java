/**
 * SeatType.java
 * 
 * List of seat types 
 */


package com.intheatersnow.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SeatType")
public class SeatType {
	@Id
	@Column(name="SeatType")
	private String seatType;
	
	public SeatType() {
	}
	
	public SeatType(String seatType) {
		this.seatType = seatType;
	}
		
	public String getSeatType() {
		return this.seatType;
	}
	
	public void setSeatType(String seatType) {
		this.seatType  = seatType;
	}
}
