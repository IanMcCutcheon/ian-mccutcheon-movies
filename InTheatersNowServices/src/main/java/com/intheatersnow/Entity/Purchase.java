/**
 * Purchase.java
 * 
 * Tracks the details of a purchase including the barcode for a ticket and 
 * whether it has been used yet or not.
 * 
 * Note for this simulation we are using UUIDs to represent barcodes since the
 * java library will generate random UUIDs.
 */

package com.intheatersnow.Entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Purchase")
public class Purchase implements IPurchase {
	@Id
	@Column(name="idPurchase")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="ticketUsed")
	private boolean ticketUsed;
	
	@Column(name="barcode")
	private String barcode;
	
	@Column(name="Showtime_idShowtime")
	private long showtimeId;
	
	/**
	 * Set the barcode when the purchase is created and indicate it has not
	 * been used yet.
	 */
	public Purchase() {
		this.barcode = UUID.randomUUID().toString();
		this.ticketUsed = false;
	}
	
	public Purchase(long showtimeId) {
		this();
		this.showtimeId = showtimeId;
	}
		
	
	/**
	 * Indicate the ticket has been used.  Once used it is always marked as 
	 * being used.
	 */
	public void setUsed() {
		this.ticketUsed = true;
	}

	/**
	 * Returns the barcode associated with this purchase.
	 */
	public String getBarcode() {
		return this.barcode;
	}

	/**
	 * Returns an indication as to whether the ticket has been used or not.
	 */
	public boolean isUsed() {
		return this.ticketUsed;
	}

	/**
	 * Sets the unique id associated with this purchase.
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the unique id associated with this purchase.
	 */
	public long getId() {
		return this.id;
	}
}
