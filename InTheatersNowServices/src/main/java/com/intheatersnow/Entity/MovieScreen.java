/**
 * MovieScreen.java
 * 
 * Defines a movie screen.  This includes the following characteristics:
 * - unique id for the screen
 * - name for the screen
 * - number of seats
 * - type of seats
 * - type of screen
 * - whether assisted listening devices are available or not
 * 
 * By defining a screen (rather than a theater) we allow a theater to have
 * multiple screens associated with it.
 */

package com.intheatersnow.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="MovieScreen")
public class MovieScreen implements IMovieScreen {
	@Id
	@Column(name="idMovieScreen")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="Name")
	private String name;
	
	@ManyToOne(targetEntity=SeatType.class)
	@JoinColumn(name="SeatType_SeatType")
	private SeatType seatType;
	
	@Column(name="NumberSeats")
	private int numberSeats;
	
	@ManyToOne(targetEntity=ScreenType.class)
	@JoinColumn(name="ScreenType_ScreenType")
	private ScreenType screenType;
	
	@Column(name="Device")
	private boolean accessibilityDevicePresent;

	public MovieScreen() {
	}
	
	/**
	 * Returns the number of seats in the theater
	 */
	public int getNumberSeats() {
		return this.numberSeats;
	}

	/**
	 * Gets the type of seats in the theater
	 */
	public String getSeatType() {
		return this.seatType.getSeatType();
	}

	/**
	 * Gets the type of screen in the theater.
	 */
	public String getScreenType() {
		return this.screenType.getScreenType();
	}

	/**
	 * Returns an indication as to whether hearing assistance devices
	 * are available in the theater or not.
	 */
	public boolean getAccessibilityDeviceAvailable() {
		return this.accessibilityDevicePresent;
	}

	/**
	 * Gets the number of seats in the theater.
	 */
	public void setNumberSeats(int numberSeats) {
		this.numberSeats = numberSeats;
	}

	/**
	 * Sets the type of seats in the theater.
	 */
	public void setSeatType(SeatType seatType) {
		this.seatType = seatType;
	}

	/**
	 * Sets the screen type in the theater.
	 */
	public void setScreenType(ScreenType screenType) {
		this.screenType = screenType;
	}

	/**
	 * Sets whether hearing assistance devices are available in the theater or not.
	 */
	public void setDevice(boolean accessibilityDevicePresent) {
		this.accessibilityDevicePresent = accessibilityDevicePresent;		
	}

	/**
	 * Gets the unique id associated with the theater.
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * Sets the unique id associated with the theater.
	 */
	public void setId(long id) {
		this.id = id;	
	}

	/**
	 * Gets the name for the theater.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name for the theater.
	 */
	public void setName(String name) {
		this.name = name;
	}
}
