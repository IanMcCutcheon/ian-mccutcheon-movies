/**
 * ScreenType.java
 * 
 * List of screen types 
 */


package com.intheatersnow.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ScreenType")
public class ScreenType {
	@Id
	@Column(name="ScreenType")
	private String screenType;
	
	public ScreenType() {
	}
	
	public ScreenType(String screenType) {
		this.screenType = screenType;
	}
		
	public String getScreenType() {
		return this.screenType;
	}
	
	public void setscreenType(String screenType) {
		this.screenType  = screenType;
	}
}
