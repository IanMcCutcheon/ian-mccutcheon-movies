/**
 * TicketType.java
 * 
 * List of different types of tickets that can be purchased.
 */

package com.intheatersnow.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TicketType")
public class TicketType {
	@Id
	@Column(name="ticketType")
	private String type;

	public TicketType() {
	}
	
	public TicketType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return this.type;
	}
}
