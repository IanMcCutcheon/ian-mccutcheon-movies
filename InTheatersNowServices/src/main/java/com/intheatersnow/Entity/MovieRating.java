/**
 * MovieRating.java
 * 
 * List of movie ratings 
 */


package com.intheatersnow.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RatingName")
public class MovieRating {
	@Id
	@Column(name="RatingStr")
	private String rating;

	public MovieRating() {
	}
	
	public MovieRating(String rating) {
		this.rating = rating;
	}
	
	public String getRating() {
		return this.rating;
	}
	
	public void setRating(String rating) {
		this.rating  = rating;
	}
}
