/**
 * Class which defines a movie.  This includes the following:
 * - title
 * - synopsis
 * - running time
 * - release date
 * - rating
 * - rating explanation
 * - genres
 */
package com.intheatersnow.Entity;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="Movie")
public class Movie implements IMovie {
	@Id
	@Column(name="idMovie")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="Title")
	private String title;
	
	@Column(name="Synopsis")
	private String synopsis;
	
	@Column(name="ReleaseDate")
	@Temporal(TemporalType.DATE)
	private Calendar releaseDate;

	@Column(name="RatingExplaination")
	private String ratingExplaination;
	
	@Column(name="RunningTime")
	private long runningTime;
	
	@ManyToOne(targetEntity=MovieRating.class)
	@JoinColumn(name="RatingName_RatingStr")
	private MovieRating rating;
	
	/* The order of the following join is important.  The movie must be first
	 * and the genre second.  I suppose this is because we are in the movie
	 * table so this is the id it uses.
	 * 
	 * Note use FecthMode.SELECT to get rid of duplicates that arise from the
	 * multiple genres assigned to each movie.  I suppose this has significant
	 * performance issues but for now we'll deal with it.
	 * 
	 */
	@ManyToMany(targetEntity=GenreName.class, fetch=FetchType.EAGER)
	@JoinTable(name="Genre", joinColumns = { @JoinColumn(name = "Movie_idMovie", nullable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "GenreName_GenreString", nullable = false) })
	@Fetch(FetchMode.SELECT)
	private Set<GenreName> genres;
	
	
	public Movie() {
		this.id = 0;
		this.title = "Undefined";
		this.synopsis = "Undefined";
		this.releaseDate = Calendar.getInstance();
		this.ratingExplaination = "";
		this.runningTime = 0;
		this.genres = new HashSet<GenreName>();
		this.rating = new MovieRating("Not Rated");
	}
		
	/**
	 * Returns the unique id associated with the movie
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * Returns the movie's title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Returns the movie's release date
	 */
	public Calendar getReleaseDate() {
		return this.releaseDate;
	}
	
	/**
	 * Returns the movie's rating
	 */
	public String getRating() {
		return this.rating.getRating();
	}
	
	public void setRating(MovieRating rating) {
		this.rating = rating;
	}
	
	public void addGenre(GenreName genre) {
		if(genres == null){
			genres = new HashSet<GenreName>();
		}
		
		genres.add(genre);
	}
	
	/**
	 * Returns the list of genres for the movie
	 */
	public Set<GenreName> getGenres() {
		return this.genres;
	}

	/**
	 * Returns the explanation for the movie's rating
	 */
	public String getRatingExplanation() {
		return this.ratingExplaination;
	}

	/**
	 * Returns the synopsis
	 */
	public String getSynopsis() {
		return this.synopsis;
	}

	/**
	 * Returns the running time in minutes
	 */
	public long getRunningTime() {
		return this.runningTime;
	}

	/**
	 * Sets the unique id used to identify the particular movie
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Set the title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Set the release date
	 */
	public void setReleaseDate(Calendar releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * Sets the rating explanation
	 */
	public void setRatingExplanation(String ratingExplanation) {
		this.ratingExplaination = ratingExplanation;
	}

	/**
	 * Sets the synopsis
	 */
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	/**
	 * Sets the running time
	 */
	public void setRunningTime(long runningTime) {
		this.runningTime = runningTime;
	}
}
