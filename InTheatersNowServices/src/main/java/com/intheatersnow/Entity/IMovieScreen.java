package com.intheatersnow.Entity;

public interface IMovieScreen {
	public int getNumberSeats();
	public String getSeatType();
	public String getScreenType();
	public boolean getAccessibilityDeviceAvailable();
	public void setNumberSeats(int numberSeats);
	public void setSeatType(SeatType seatType);
	public void setScreenType(ScreenType screenType);
	public void setDevice(boolean accessibilityDevicePresent);
	public long getId();
	public void setId(long id);
	public String getName();
	public void setName(String name);
}
