package com.intheatersnow.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Ticket")
public class Ticket implements ITicket {
	@Id
	@Column(name="idTicket")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="price")
	private float price;
	
	@ManyToOne(targetEntity=TicketType.class)
	@JoinColumn(name="TicketType_ticketType")
	private TicketType type;
	
	/**
	 * Returns the unique id associated with the ticket
	 */
	public long getId() {
		return this.id;
	}
	
	/**
	 * Sets the unique id associated with the ticket
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Returns the price of the ticket
	 */
	public float getPrice() {
		return this.price;
	}

	/**
	 * Sets the price of the ticket
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * Get the type of the ticket
	 */
	public TicketType getTicketType() {
		return type;
	}

	/**
	 * Sets the type of the ticket
	 */
	public void setTicketType(TicketType type) {
		this.type = type;
	}
}
