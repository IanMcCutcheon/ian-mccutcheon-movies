/**
 * Theater.java
 * 
 * Defines a movie theater.  This includes the following characteristics:
 * - unique id for the theater
 * - name for the theater
 * - address of the theater
 * - phone number for the theater
 * - number of screens associated with the theater
 * 
 * The individual screens are maintained in a separate class so that we can
 * support theaters from 1 to any number of screens.
 */

package com.intheatersnow.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.intheatersnow.Address;

@Entity
@Table(name="Theater")
public class Theater implements ITheater {
	@Id
	@Column(name="idTheater")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
		
	@Column(name="Name")
	private String name;
	@Column(name="PhoneNumber")
	private String phoneNumber;
	@Column(name="Street1")
	private String street1;
	@Column(name="Street2")
	private String street2;
	@Column(name="City")
	private String city;
	@Column(name="State")
	private String state;
	@Column(name="ZipCode")
	private int zipCode;
		
	
	/* The order of the following join is important.  The movie must be first
	 * and the genre second.  I suppose this is because we are in the movie
	 * table so this is the id it uses.
	 */
	@ManyToMany(targetEntity=MovieScreen.class, fetch=FetchType.EAGER)
	@JoinTable(name="TheaterScreens", joinColumns = { @JoinColumn(name = "Theater_idTheater", nullable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "MovieScreen_idMovieScreen", nullable = false) })
	@Fetch(FetchMode.SELECT)
	private List<IMovieScreen> screens;

	public Theater() {
	}
			
	/**
	 * Returns the unique id associated with the theater.
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * Returns the name of the theater.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the address of the theater.
	 */
	public Address getAddress() {
		Address address = new Address();
		address.setStreet1(this.street1);
		address.setStreet2(this.street2);
		address.setCity(this.city);
		address.setState(this.state);
		address.setZipCode(this.zipCode);
		
		return address;
	}

	/**
	 * Returns the phone number of the theater.
	 */
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * Returns the number of screens available at the theater.
	 */
	public int getNumberScreens() {
		return this.screens.size();
	}

	/**
	 * Returns the list of screens that are associated with the theater.
	 */
	public List<IMovieScreen> getScreens() {
		return this.screens;
	}
	
	/**
	 * Sets a unique id associated with the theater
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Sets the name of the theater
	 */
	public void setName(String name) {
		this.name = name;		
	}

	/**
	 * Sets the street1 for the theater
	 */
	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	/**
	 * Sets the street2 for the theater
	 */
	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	/**
	 * Sets the city for the theater
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Sets the state for the theater
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Sets the zip code for the theater
	 */
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Sets the phone number for the theater
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Adds a screen to the theater.
	 */
	public void addScreen(IMovieScreen movieScreen) {
		if(this.screens == null){
			this.screens = new ArrayList<IMovieScreen>();
		}
		
		this.screens.add(movieScreen);
	}
	
	/**
	 * Returns the specified screen associated with the theater.
	 * 
	 * @param sceenId: Id of the screen to find
	 * @return Requested screen, null if not found
	 */
	public IMovieScreen getScreenById(long screenId) {
		if (this.screens != null) {
			for (IMovieScreen screen : screens) {
				if (screen.getId() == screenId) {
					return screen;
				}
			}
		}
			
		return null;
	}
}
