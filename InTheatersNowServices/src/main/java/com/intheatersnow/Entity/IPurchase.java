package com.intheatersnow.Entity;

public interface IPurchase {
	public void setUsed();
	public String getBarcode();
	public boolean isUsed();
	public void setId(long id);
	public long getId();
}
