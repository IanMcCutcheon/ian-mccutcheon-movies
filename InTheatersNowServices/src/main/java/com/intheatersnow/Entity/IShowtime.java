package com.intheatersnow.Entity;

import java.util.Calendar;
import java.util.List;

public interface IShowtime {
	public void setShowtimeId(long showtimeId);
	public long getShowtimeId();
	public void setTheaterId(long theaterId);
	public long getTheaterId();
	public void setMovieId(long movieId);
	public long getMovieId();
	public void setScreenId(long screenId);
	public long getScreenId();
	public Calendar getShowtime();
	public void setShowtime(Calendar showtime);
	public void addTicket(ITicket ticket);
	public List<ITicket> getTickets();
	public void puchaseTickets(int numberTickets);
	public int ticketsSold();
}
