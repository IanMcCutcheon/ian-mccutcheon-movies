/**
 * ShowtimeRepository.java
 * 
 * Service which maintains the list of showtimes.  A showtime is a particular instance 
 * of the showing of a movie and is characterized by:
 * - theater where the movie is being shown
 * - which screen the movie is being shown on
 * - the date and time the movie is being shown
 * - the types of tickets and prices available for the showing
 * - the number of tickets sold for the showing
 */


package com.intheatersnow.Entity;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.intheatersnow.Entity.Ticket;

@Entity
@Table(name="Showtime")
public class Showtime implements IShowtime {
	@Id
	@Column(name="idShowtime")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long showtimeId;
	
	@Column(name="Theater_idTheater")
	private long theaterId;
	@Column(name="Movie_idMovie")
	private long movieId;
	@Column(name="TheaterScreens_idTheaterScreens")
	private long screenId;

	@Column(name="showing")
	private Calendar showing;
	
	/* The order of the following join is important.  The showtime must be first
	 * and the tickets second.  I suppose this is because we are in the movie
	 * table so this is the id it uses.
	 */
	@ManyToMany(targetEntity=Ticket.class, fetch=FetchType.EAGER)
	@JoinTable(name="AvailableTickets", joinColumns = { @JoinColumn(name = "Showtime_idShowtime", nullable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "Ticket_idTicket", nullable = false) })
	@Fetch(FetchMode.SELECT)
	private List<ITicket> tickets;
	
	@Column(name="ticketsSold")
	private int ticketsSold;

	
	public Showtime() {
	}
	
	public void setShowtimeId(long showtimeId) {
		this.showtimeId = showtimeId;
	}

	public long getShowtimeId() {
		return this.showtimeId;
	}

	public void setTheaterId(long theaterId) {
		this.theaterId = theaterId;		
	}

	public long getTheaterId() {
		return this.theaterId;
	}

	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}

	public long getMovieId() {
		return this.movieId;
	}

	public void setScreenId(long screenId) {
		this.screenId = screenId;
	}

	public long getScreenId() {
		return this.screenId;
	}

	public Calendar getShowtime() {
		return this.showing;
	}

	public void setShowtime(Calendar showtime) {
		this.showing = showtime;
	}

	public void addTicket(ITicket ticket) {
		if (this.tickets == null) {
			this.tickets = new ArrayList<ITicket>();
		}
		
		this.tickets.add(ticket);
	}

	public List<ITicket> getTickets() {
		return this.tickets;
	}

	/**
	 * Purchases the specified number of tickets for this showtime
	 */
	public void puchaseTickets(int numberTickets) {
		this.ticketsSold += numberTickets;
	}

	/**
	 * Returns the number of tickets sold for this showtime
	 */
	public int ticketsSold() {
		return this.ticketsSold;
	}
}
