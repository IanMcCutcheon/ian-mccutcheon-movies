/**
 * Class which defines a custom exception handler so that an error code and
 * message can be returned explaining the source of the error.
 */
package com.intheatersnow.repositoryException;

@SuppressWarnings("serial")
public class INTNException extends RuntimeException {
	private ErrorCode errorCode;

	public INTNException(ErrorCode code, String message, Throwable throwable) {
		super(message, throwable);
		this.errorCode = code;
	}
	
	public INTNException(ErrorCode code, String message) {
		super(message);
		this.errorCode = code;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

}
