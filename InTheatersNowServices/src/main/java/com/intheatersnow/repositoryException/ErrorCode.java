/**
 * List of error codes defined for the application
 */

package com.intheatersnow.repositoryException;

public enum ErrorCode {
	NOT_FOUND,
	INVALID_FIELD,
	MISSING_DATA,
	INTERNAL_ERROR
}
