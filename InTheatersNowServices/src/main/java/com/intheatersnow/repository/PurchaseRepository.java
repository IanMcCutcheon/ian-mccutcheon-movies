/**
 * PurchaseRepository.java
 * 
 * Provides services to purchase tickets for a showtime and validate that
 * previously purchased tickets are valid.
 */

package com.intheatersnow.repository;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.intheatersnow.Entity.IPurchase;
import com.intheatersnow.Entity.Purchase;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

@Repository
public class PurchaseRepository {
	@Autowired
    private SessionFactory sessionFactory;
	
	@Autowired
	private ShowtimeRepository showtimeRepository;
	
	/**
	 * Adds a new purchase to the database 
	 * 
	 * @param purchase, purchase to add
	 * @return id of the added purchase
	 */
	private long add(IPurchase purchase) {
		return (Long) this.sessionFactory.getCurrentSession().save(purchase);
	}
	
	/**
	 * Gets the purchase details given its id
	 * @param purchaseId, id for the requested purchase
	 * @return purchase corresponding to the id or null if not found
	 */
	@Transactional
	public IPurchase getPurchaseById(long purchaseId) {
		return (Purchase) this.sessionFactory.getCurrentSession().get(Purchase.class, purchaseId);
	}
	
	/**
	 * Updates a preexisitng purchase
	 * @param purchase, purchase to update
	 * @return true if the purchase was updated, false otherwise (non existing purchase)
	 */
	//@Transactional//at method level
	private boolean update(IPurchase purchase) {
		IPurchase existingPurchase = this.getPurchaseById(purchase.getId());
		boolean purchasePresent = existingPurchase != null; 
		if (!purchasePresent) {
			System.out.println("Can't update purchase not in database");
		}
		else {
			this.sessionFactory.getCurrentSession().update(purchase);
		}
		
		return purchasePresent;
	}
	
	/**
	 * Purchase tickets for a particular showtime.
	 * @param showtimeId: showtime to purchase tickets for
	 * @param numberTickets: The number of tickets to buy
	 * @return If successful a barcode for each ticket purchased
	 */
	@Transactional
	public List<String> purchaseTickets(long showtimeId, int numberTickets) {
		List<String>barcodes = new ArrayList<String>();
		IPurchase purchase;
		
		System.out.println("Checking for tickets for showtime " + showtimeId);
		if (showtimeRepository.getAvailableTickets(showtimeId) >= numberTickets) {
			System.out.println("Tickets are available");
			if (showtimeRepository.purchaseTickets(showtimeId, numberTickets)) {
				System.out.println("Purchased the tickets");
				/* We bought the tickets, so generate the barcodes */
				for (int i = 0; i < numberTickets; ++i) {
					purchase = new Purchase(showtimeId);
					this.add(purchase);
					barcodes.add(purchase.getBarcode());
				}
			}
			else {
				System.out.println("Unable to purchase tickets");
			}
		}
		else {
			System.out.println("No tickets are available");
		}
				
		return barcodes;
	}

	/**
	 * Checks the barcode to insure it is registered with the system and has not been used
	 * yet.  Once it is checked it is marked as used so subsequent attempts will fail.
	 * @param barcode: Barcode to check
	 * @return true if barcode is valid and has not been used, false otherwise
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean validateTicket(String barcode) {
		System.out.println("Attempting to valida ticket " + barcode);
		
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Purchase.class)
				.add(Restrictions.like("barcode", barcode, MatchMode.EXACT));		
		List<IPurchase> purchases = crit.list();
		if (purchases.size() > 1) {
			System.out.println("Internal error, more than one purchase has the same barcode");
			throw new INTNException(ErrorCode.INTERNAL_ERROR, "more than one purchase has the same barcode");
		}
		
		if (purchases.size() == 1) {
			if (!purchases.get(0).isUsed()) {
				purchases.get(0).setUsed();
				if (!this.update(purchases.get(0))) {
					System.out.println("Error updating purchase " + purchases.get(0).getId());
					throw new INTNException(ErrorCode.INTERNAL_ERROR, "Error updating purchase");
				}
		
				return true;
			}
		}
		else {
			System.out.println("Barcode not recognized, potential fake ticket!");
			throw new INTNException(ErrorCode.NOT_FOUND, "Barcode not recognized");
		}
			
		return false;
	}
}
