/**
 * ShowtimeService.java
 * 
 * Service which maintains the list of showtimes.  A showtime is a particular instance 
 * of the showing of a movie and is characterized by:
 * - theater where the movie is being shown
 * - which screen the movie is being shown on
 * - the date and time the movie is being shown
 * - the types of tickets and prices available for the showing
 * - the number of tickets sold for the showing
 */

package com.intheatersnow.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.intheatersnow.Entity.ITicket;
import com.intheatersnow.Entity.IMovie;
import com.intheatersnow.Entity.IMovieScreen;
import com.intheatersnow.Entity.IShowtime;
import com.intheatersnow.Entity.ITheater;
import com.intheatersnow.Entity.Showtime;
import com.intheatersnow.Entity.TicketType;

@Repository
public class ShowtimeRepository {
	@Autowired
    private SessionFactory sessionFactory;
	
	@Autowired
	private TheaterRespository theaterRepository;
	
	@Autowired
	private MovieRepository movieRepository;

	/**
	 * Gets the showtime details given a showtime id
	 * @param showtimeId, showtime to search for
	 * @return showtime details corresponding to the provided id, null if not found
	 */
	@Transactional
	public Showtime getShowtimeById(long showtimeId) {
		return (Showtime) this.sessionFactory.getCurrentSession().get(Showtime.class, showtimeId);
	}
	
	/**
	 * Add a new showtime to the list of showtimes and assign it a 
	 * unique id.
	 * 
	 * @param showtime: Showtime to add
	 */
	@Transactional
	public long addShowtime(IShowtime showtime) {
		return (Long) this.sessionFactory.getCurrentSession().save(showtime);
	}	
	
	/**
	 * Updates the details for an existing showtime
	 * @param showtime to be updated
	 * @return true if the showtime was updated, false otherwise (not in the database)
	 */
	@Transactional
	public boolean update(IShowtime showtime) {
		boolean showtimePresent = true;
		
		IShowtime existingShowtime = this.getShowtimeById(showtime.getShowtimeId());
		if (existingShowtime == null) {
			System.out.println("Can't update showtime not in database");
			showtimePresent = false;
		}
		else {
			this.sessionFactory.getCurrentSession().update(showtime);
		}
		
		return showtimePresent;
	}
	
	/**
	 * Returns a list of showtimes for a particular movie playing at a particular
	 * theater on a particular date.
	 * 
	 * @param theater: Id of the theater to check
	 * @param movie: Id of the movie to check
	 * @param date: Date to check
	 * @return: List of showtimes
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<IShowtime> getShowtimesByTheaterMovieDate(long theaterId, long movieId, Calendar date) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		
		fromDate.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		toDate.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Showtime.class)
				.add(Restrictions.eq("theaterId", theaterId))
				.add(Restrictions.eq("movieId",  movieId))
      			.add(Restrictions.between("showing",  fromDate, toDate));
		List<IShowtime> showtimes = crit.list();		
		
		return showtimes;
	}
	
	
	/**
	 * Returns a list of showtimes playing at a particular theater on a 
	 * given date.
	 * 
	 * @param theater: Id of the theater to check
	 * @param date: Date to check
	 * @return: List of showtimes
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<IShowtime> getShowtimesByTheaterDate(long theaterId, Calendar date) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();

		fromDate.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		toDate.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Showtime.class)
				.add(Restrictions.eq("theaterId", theaterId))
      			.add(Restrictions.between("showing",  fromDate, toDate));
		List<IShowtime> showtimes = crit.list();		
		
		return showtimes;
	}
	
	/**
	 * Returns a list of movies playing at a particular theater on a 
	 * given date.
	 * 
	 * @param theater: Id of the theater to check
	 * @param date: Date to check
	 * @return: List of movies
	 */
	@Transactional
	public List<IMovie> getMoviesByTheaterAndDate(long theaterId, Calendar date) {
		List<IMovie>results = new ArrayList<IMovie>();
		IMovie movie;
		
		List<IShowtime>showtimes = getShowtimesByTheaterDate(theaterId, date);
		for (IShowtime showtime : showtimes) {
			/* Only add the movie once */
			movie = movieRepository.getMovieById(showtime.getMovieId());
			if (!results.contains(movie)) {
				results.add(movie);
			}
		}
	
		return results;
	}
	
	/**
	 * Returns all the theaters showing the specified movie
	 * @param movieId: Id of movie to check
	 * @return List of theaters where the movie is playing
	 */
	@Transactional
	@SuppressWarnings("unchecked")
	public List<ITheater> getTheatersShowingMovie(long movieId) {
		List<ITheater>results = new ArrayList<ITheater>();
		ITheater theater;
	
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Showtime.class)
				.add(Restrictions.eq("movieId",  movieId));
		List<IShowtime> showtimes = crit.list();		
	
		for (IShowtime showtime : showtimes) {
			/* Only add the theater once */
			theater = theaterRepository.getTheaterById(showtime.getTheaterId());
			if (!results.contains(theater)) {
				results.add(theater);
			}
		}
		
		return results;
	}
	
	/**
	 * Returns the number of seats available for a particular showtime
	 * 
	 * @param showtimeId: Id of the showtime to check
	 * @return The number of tickets available for purchase
	 */
	@Transactional
	public int getAvailableTickets(long showtimeId) {
		try {
			IShowtime showtime = getShowtimeById(showtimeId);
			if (showtime == null) {
				System.out.println("Unable to load showtime " + showtimeId);
			}
			else {
				ITheater theater = theaterRepository.getTheaterById(showtime.getTheaterId());
				if (theater == null) {
					System.out.println("There is no theater assigned for this showtime!");
				}
				else {
					IMovieScreen screen = theater.getScreenById(showtime.getScreenId());
					if (screen != null) {
						int numberSeats = screen.getNumberSeats();
						return numberSeats - showtime.ticketsSold();
					}
					else {
						System.out.println("There is no screen assigned to this showtime!");;
					}
				}
			}
		}
		catch (Exception ex) {
			System.out.println("Exception " + ex.getMessage());
		}
		
		return 0;
	}
	
	/**
	 * Gets the ticket price associated with a particular showtime
	 * and ticket type.  Returns -1 if the ticket is not available.
	 * 
	 * @param showtimeId: Id of the showtime to check
	 * @param type: Type of ticket to buy
	 * @return The price of the ticket for that showtime
	 */
	public float getTicketPrice(long showtimeId, TicketType type) {
		IShowtime showtime = getShowtimeById(showtimeId);
		List<ITicket>tickets = showtime.getTickets();
				
		for (ITicket ticket : tickets) {
			if (ticket.getTicketType() == type) {
				return ticket.getPrice();
			}
		}
		
		/* Indicate the ticket is not available */
		return -1.0f;
	}
	
	/**
	 * Allows the customer to purchase a specified number of tickets for 
	 * a particular showtime.  Insures the showtime is valid (valid id and
	 * movie hasn't started yet) and there are enough seats.  Otherwise the
	 * ticket can't be purchased. 
	 * 
	 * @param showtimeId: Id of the showtime to check
	 * @param numberTickets: number of tickets wanting to buy
	 * @return true if the ticket was able to be purchased
	 */
	@Transactional
	public boolean purchaseTickets(long showtimeId, int numberTickets) {
		
		IShowtime showtime = getShowtimeById(showtimeId);
		if (showtime != null) {
			System.out.println("Found the showtime: " + showtimeId);
			/* Insure the show hasn't started yet. If it has don't sell the
			 * ticket (although I suppose we could allow ticket sales up to 
			 * say 30 min after the movie starts (lots of trailers!).
			 */
			if (showtime.getShowtime().after(Calendar.getInstance())) {
				int availableSeats = getAvailableTickets(showtimeId);

//				System.out.println("available seats: " + availableSeats + "# " + numberTickets);
		
				/* 
				 * Should really hold the tickets until the credit card is charged and then
				 * indicates the seats are sold but we'll deal with the simple case for now.
				 * If there are not enough available seats then reject the purchase.
				 */
				if (availableSeats >= numberTickets) {
					showtime.puchaseTickets(numberTickets);
					this.update(showtime);
					return true;
				}
			}
		}
		else {
			System.out.println("Could not find showtime: " + showtimeId);
		}
		
		return false;
	}
}
