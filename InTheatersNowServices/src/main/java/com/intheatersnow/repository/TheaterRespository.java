/**
 * TheaterRepository.java
 * 
 * Theaters show films and have a name, address and phone number.  In addition
 * a theater has one or more screens associated with it.  This class manages
 * the theaters and their screens.
 * 
 */package com.intheatersnow.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.intheatersnow.Entity.IMovieScreen;
import com.intheatersnow.Entity.ITheater;
import com.intheatersnow.Entity.Theater;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

@Repository
public class TheaterRespository {
	@Autowired
    private SessionFactory sessionFactory;

	/**
	 * Adds a theater to the database
	 * 
	 * @param theater to add
	 */
	public long addTheater(ITheater theater) {
		return (Long) this.sessionFactory.getCurrentSession().save(theater);
	}
	
	/**
	 * Returns all theaters in the database
	 * 
	 * @return List of all theaters in the database
	 */
	@Transactional
	@SuppressWarnings("unchecked")
	public List<ITheater> getAllTheaters() {
		List<ITheater> theaters = this.sessionFactory.getCurrentSession().createCriteria(Theater.class).list();
		return theaters;
	}

	/**
	 * Returns the theater that corresponds to the provided id. If
	 * no theater matches then returns null.
	 * 
	 * @param theaterId id of the theater to find
	 * @return theater corresponding to the id or null if not found
	 */	
	@Transactional//at method level
	public ITheater getTheaterById(long theaterId) {
		return (ITheater) this.sessionFactory.getCurrentSession().get(Theater.class, theaterId);
	}
	
	/**
	 * Returns any theaters that are located in the specified zip code.
	 * 
	 * @param zip code: zip code to search within
	 * @return List of theaters that match the specified zip code
	 */
	/* Figure out what this does */
	@Transactional//at method level
	@SuppressWarnings("unchecked")
	public List<ITheater> getTheaterByZipCode(int zipCode) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Theater.class)
			.add(Restrictions.eq("zipCode", zipCode));		
		List<ITheater> theaters = crit.list();
		return theaters; 
	}	
	
	/**
	 * Returns any theaters that match the specified name.  A 
	 * partial match is allowed.
	 * 
	 * @param name: name of the theater to search for
	 * @return List of theaters that match the specified name
	 */
	/* Figure out what this does */
	@Transactional//at method level
	@SuppressWarnings("unchecked")
	public List<ITheater> getTheaterByName(String name) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Theater.class)
			.add(Restrictions.like("name", name, MatchMode.ANYWHERE));		
		List<ITheater> theaters = crit.list();
		return theaters; 
	}	

	/**
	 * Returns any theaters that are located in the specified city/state code.
	 * 
	 * @param city: city to search within
	 * @param state: state to search within
	 * @return List of theaters that match the specified city/state 
	 */
	/* Figure out what this does */
	@Transactional//at method level
	@SuppressWarnings("unchecked")
	public List<ITheater> getTheaterByCityAndState(String city, String state) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Theater.class);
		if (!city.isEmpty()) {
			crit.add(Restrictions.like("city", city, MatchMode.EXACT));
		}
		
		if (!state.isEmpty()) {
			crit.add(Restrictions.like("state", state, MatchMode.EXACT));
		}
		
		List<ITheater> theaters = crit.list();
		return theaters; 
	}	
	
	
	/**
	 * Updates the details associated with a theater.  If the theater is 
	 * not in the database no changes are made.
	 * 
	 * @param theaterId id of the theater to update
	 * @param updateTheater new theater settings
	 * @return true of the theater was updated, false otherwise
	 */
	//@Transactional//at method level
	public boolean update(ITheater theater) {
		boolean theaterPresent = true;
		
		ITheater existingTheater = this.getTheaterById(theater.getId());
		if (existingTheater == null) {
			System.out.println("Can't update theater not in database");
			throw new INTNException(ErrorCode.NOT_FOUND, "No such theater");	
		}
		else {
			this.sessionFactory.getCurrentSession().update(theater);
		}
		return theaterPresent;
	}
	
	/**
	 * Adds a screen that is to be associated with this theater.
	 * 
	 * @param theaterId id of the owning theater
	 * @param screen to add to the theater
	 */
	public void addScreen(long theaterId, IMovieScreen screen) {
		ITheater theater;
		
		theater = getTheaterById(theaterId);
		if (theater != null) {
			theater.addScreen(screen);
			update(theater);
		}
	}
}
