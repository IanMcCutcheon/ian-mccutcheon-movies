/**
 * UserRepository.java
 * 
 * Maintains the list of users known to the system and provides the ability for the user to 
 * purchase tickets.
 */

package com.intheatersnow.repository;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.intheatersnow.Entity.IUser;
import com.intheatersnow.Entity.TicketType;
import com.intheatersnow.Entity.User;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

@Repository
public class UserRepository {
	private static final int ACCOUNT_LENGTH = 9;
	
	@Autowired
    private SessionFactory sessionFactory;

	@Autowired
	private PurchaseRepository purchaseRepository;

	/**
	 * Adds a user to the user database as long as the account is unique.
	 */
	@Transactional
	public long addUser(IUser user) {
		long userId = -1;
		
		/* Don't allow a user with no account to be entered */
		if(StringUtils.isEmpty(user.getAccount())) {
			System.out.println("Empty account number!");
			throw new INTNException(ErrorCode.MISSING_DATA, "No account number provided");	
		}
		
		if (!user.getAccount().matches("[0-9]{" + ACCOUNT_LENGTH  + "}")) {
			System.out.println("Account number not correct format");
			throw new INTNException(ErrorCode.MISSING_DATA, "Account number must be nine digits long");	
		}
		
		/* See if a user with the same account number already exists */
		IUser existingUser = getUserByAccount(user.getAccount());
		if (existingUser == null) {
			System.out.println("Account number not used so adding user");
			userId = (Long)this.sessionFactory.getCurrentSession().save(user);
		}
		else {
			System.out.println("Account number already in use, ignoring request to add");
			throw new INTNException(ErrorCode.INVALID_FIELD, "Account number already used");	
		}			
		
		return userId;
	}
	
	/**
	 * Returns the complete list of users in the system.
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<IUser> getAllUsers() {
		List<IUser> users = this.sessionFactory.getCurrentSession().createCriteria(User.class).list();
		return users;
	}
	
	/**
	 * Updates the user account details.
	 */
	//@Transactional//at method level
	public boolean update(IUser user) {
		IUser existingUser = this.getUserById(user.getId());
		boolean userPresent = existingUser != null;
		if (!userPresent) {
			System.out.println("Can't update user not in database");
			throw new INTNException(ErrorCode.NOT_FOUND, "User not registered");	
		}
		else {
			this.sessionFactory.getCurrentSession().update(user);
		}
		
		return userPresent;
	}
	
	/**
	 * Gets the number of users in the database
	 * 
	 * @return number of users in database
	 */
	public long getNumberUsers() {
		long number = (Long)this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.setProjection(Projections.rowCount()).uniqueResult();
		return number; 
	}	
	
	/**
	 * Returns the user whose id matches the provided id.
	 * Returns null if the user could not be found.
	 * 
	 */
	@Transactional//at method level
	public IUser getUserById(long userId) {
		return (IUser) this.sessionFactory.getCurrentSession().get(User.class, userId);
	}
	
	/**
	 * Returns the user whose account matches the provided account number.
	 * Returns null if the user could not be found.
	 */
	/* Figure out what this does */
	@Transactional//at method level
	public IUser getUserByAccount(String accountNumber) {
		System.out.println("Query for account by account number: " + accountNumber);
		
		if(StringUtils.isEmpty(accountNumber)) {
			System.out.println("Empty account number!");
			throw new INTNException(ErrorCode.MISSING_DATA, "no account number provided");	
		}
		else {
			Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(User.class)
					.add(Restrictions.like("account", accountNumber, MatchMode.EXACT));	
			IUser user = (User)crit.uniqueResult();
			return user;
		}
	}	
	
	/**
	 * Purchases tickets for the user for a specific show time.  Since we can't validate the 
	 * user's credit card, for now assume that as long as they have a valid credit card the 
	 * transaction will be successful.
	 */
	@Transactional//
	public List<String> purchaseTickets(String account, long showtimeId, List<TicketType> tickets) {
		List<String> barcodes = new ArrayList<String>();
		IUser user;
		
		System.out.println("Purchase tickets for account " + account + " and showtime: " + showtimeId);
		user = this.getUserByAccount(account);
		if (user != null) {
			System.out.println("Valid user account was found");
			
			/* We can't charge the credit card so just check to insure the card is valid and if 
			 * so assume the user is good for the money.
			 */
			if (user.isCreditCardValid()) {
				System.out.println("Credit card is valid");
				int numberTickets = tickets.size();
				barcodes = purchaseRepository.purchaseTickets(showtimeId,  numberTickets);
				System.out.println("Bought tickets: " + barcodes.size());
			}
			else {
				System.out.println("User's credit card is invalid or expired");
				throw new INTNException(ErrorCode.INVALID_FIELD, "Credit Card expired or invalid");
			}
		}
		else {
			System.out.println("Tickets can't be purchased, user account not valid: " + account);
			throw new INTNException(ErrorCode.INVALID_FIELD, "Account does not exist");
		}
		
		return barcodes;
	}
}
