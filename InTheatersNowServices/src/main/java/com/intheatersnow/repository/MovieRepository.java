/**
 * MovieRepository.java
 * 
 * Maintains the list of movies in the database and allows the system to 
 * query them by title or id.
 */

package com.intheatersnow.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.intheatersnow.Entity.GenreName;
import com.intheatersnow.Entity.IMovie;
import com.intheatersnow.Entity.Movie;
import com.intheatersnow.Entity.MovieRating;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

@Repository
public class MovieRepository {	
	
	private static final int MAX_MOVIE_TITLE_LENGTH = 256;
	private static final int MIN_MOVIE_TITLE_LENGTH = 1;
	private static final int MAX_SYNOPSIS_LENGTH = 512;
	private static final int MAX_RATING_EXPLANATION_LENGTH = 256;
	
	@Autowired
    private SessionFactory sessionFactory;

	/**
	 * Validates that the given rating is valid
	 * @param rating to check
	 */
	@SuppressWarnings("unchecked")
	private void checkRating(String rating) {
		System.out.println("Checking rating: " + rating);
		
		try	{
			Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(MovieRating.class)
					.add(Restrictions.like("rating", rating, MatchMode.EXACT));		
			List<MovieRating> ratings = crit.list();
			if (ratings == null || ratings.size() == 0) {
				System.out.println("Could not find rating: " + rating);
				throw new INTNException(ErrorCode.INVALID_FIELD, "Unknown rating " + rating);
			}
		}
		catch (Exception ex) {
			System.out.println ("checkRating Exception: " + ex.getMessage());
			throw new INTNException(ErrorCode.INVALID_FIELD, "Unknown rating " + rating);
		}
	}
	
	/**
	 * Validates that the provided genre is valid
	 * @param genre to check
	 */
	@SuppressWarnings("unchecked")
	private void checkGenre(String genre) {
		System.out.println("Checking genre: " + genre);
		
		try	{
			Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(GenreName.class)
					.add(Restrictions.like("genre", genre, MatchMode.EXACT));		
			List<GenreName> genres = crit.list();
			if (genres == null || genres.size() == 0) {
				System.out.println("Could not find genre: " + genre);
				throw new INTNException(ErrorCode.INVALID_FIELD, "Unknown genre " + genre);
			}
		}
		catch (Exception ex) {
			System.out.println ("checkGenre Exception: " + ex.getMessage());
			throw new INTNException(ErrorCode.INVALID_FIELD, "Unknown genre " + genre);
		}
	}
	
	/**
	 * Validates that the fields for the movie are valid
	 * @param movie to check
	 */
	private void validateMovie(IMovie movie) {
		/* Check the title */
		String title = movie.getTitle();
		if(StringUtils.isEmpty(title) || 
				(title.length() > MAX_MOVIE_TITLE_LENGTH) ||
				(title.length() < MIN_MOVIE_TITLE_LENGTH)) {
			throw new INTNException(ErrorCode.INVALID_FIELD, "Title invalid length");
		}
		
		/* Check the synopsis */
		String synopsis = movie.getSynopsis();
		if(!StringUtils.isEmpty(synopsis) 
			&& (synopsis.length() > MAX_SYNOPSIS_LENGTH)) {
			throw new INTNException(ErrorCode.INVALID_FIELD, "Synopsis too long");
		}
		
		/* Check the rating explanation */
		String explanation = movie.getRatingExplanation();
		if(!StringUtils.isEmpty(explanation) 
			&& (synopsis.length() > MAX_RATING_EXPLANATION_LENGTH)) {
			throw new INTNException(ErrorCode.INVALID_FIELD, "Rating explanation too long");
		}
		
		/* Check the rating */
		this.checkRating(movie.getRating());
		
		/* Check the genres */
		for (GenreName genre:movie.getGenres()) {
			this.checkGenre(genre.getGenre());
		}
	}

	/**
	 * Adds a movie to the database.
	 * 
	 * @param movie to add
	 */
	@Transactional
	public long addMovie(IMovie movie) {
		this.validateMovie(movie);
		return (Long) this.sessionFactory.getCurrentSession().save(movie);
	}
	
	/**
	 * Returns the movie corresponding to the provided id
	 * 
	 * @param movieId id of the movie being requested
	 * @return the requested movie if it exists and null otherwise
	 */
	@Transactional
	public IMovie getMovieById(long movieId) {
		return (IMovie) this.sessionFactory.getCurrentSession().get(Movie.class, movieId);
	}
	
	/**
	 * Returns a list of all movies whose title matches the provided search
	 * title.  Note that partial matches are also returned.
	 * 
	 * @param searchTitle: title (partial or full) of the movie to search for
	 * @return List of movies that match search criteria
	 */
	/* Figure out what this does */
	@Transactional
	@SuppressWarnings("unchecked")
	public List<IMovie> getMovieByTitle(String title) {
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(Movie.class)
			.add(Restrictions.like("title", title, MatchMode.ANYWHERE));		
		List<IMovie> movies = crit.list();
		return movies; 
	}	
	
	/**
	 * Returns all the movies in the database
	 * 
	 * @return List of all movies in the database
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<IMovie> getAllMovies() {		
		List<IMovie> movies = this.sessionFactory.getCurrentSession().createCriteria(Movie.class).list();
		return movies;
	}
	
	/**
	 * Gets the number of movies in the database
	 * @return number of movies in the database
	 */
	public long getNumberMovies() {
		long number = (Long)this.sessionFactory.getCurrentSession().createCriteria(Movie.class)
				.setProjection(Projections.rowCount()).uniqueResult();
			return number; 
	}		
	
	/**
	 * Updates a movie in the database
	 * 
	 * @param updateMovie: Movie to update. It must already be in
	 * the database or the update is ignored.
	 * @return true if the movie was updated, false otherwise
	 */
	@Transactional
	public boolean update(IMovie movieToUpdate) {
		System.out.println("Updating movie " + movieToUpdate.getId());
		this.validateMovie(movieToUpdate);
		
//		IMovie movie = this.getMovieById(movieToUpdate.getId());
//		boolean moviePresent = (movie != null);
//		if (!moviePresent) {
//			System.out.println("Can't update movie not in database");
//			throw new INTNException(ErrorCode.NOT_FOUND, "No such movie");	
//		}
//		else {
			this.sessionFactory.getCurrentSession().update(movieToUpdate);
//		}
		
		System.out.println("Updated the movie " + movieToUpdate.getTitle());
		return true; //moviePresent;
	}
}
