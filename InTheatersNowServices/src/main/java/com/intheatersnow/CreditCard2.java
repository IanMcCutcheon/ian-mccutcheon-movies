/**
 * Keeps track of the details for a credit card
 */

package com.intheatersnow;

import java.util.Calendar;

public class CreditCard2 {
	private String number;
	private Calendar expiration;
	private int securityCode;
	
	/**
	 * Returns the credit card number
	 */
	public String getNumber() {
		return this.number;
	}
	
	/**
	 * Returns the credit card expiration date
	 */
	public Calendar getExpiration() {
		return this.expiration;
	}
	
	/**
	 * Returns the credit card security code
	 */
	public int getSecurityCode() {
		return this.securityCode;
	}

	/**
	 * Sets the credit card number
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * Sets the credit card expiration date
	 */
	public void setExpiration(Calendar expiration) {
		this.expiration = expiration;
	}

	/**
	 * Sets the security code
	 */
	public void setSecurityCode(int securityCode) {
		this.securityCode = securityCode;
	}
}
