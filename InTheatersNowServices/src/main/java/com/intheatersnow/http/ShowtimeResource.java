package com.intheatersnow.http;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intheatersnow.Entity.IShowtime;
import com.intheatersnow.httpEntity.httpShowtime;
import com.intheatersnow.repository.ShowtimeRepository;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

@Path("/showtimes")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class ShowtimeResource {
	@Autowired
	private ShowtimeRepository showtimeRepository;
		
	@GET
	@Wrapped(element="showtimes")
	public List<httpShowtime> getShowtimesByTheaterDate(@QueryParam("theater") long theaterId, @QueryParam("date")String date) {
		int availTickets;
		Calendar checkDate;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		System.out.println("Getting movies for theater " + theaterId + " on " + date);
		
		try {
			checkDate = Calendar.getInstance();
			checkDate.setTime(sdf.parse(date));
		}
		catch (Exception ex) {
			throw new INTNException(ErrorCode.INVALID_FIELD, "Date format is invalid");
		}
		
		List<IShowtime> showtimes = showtimeRepository.getShowtimesByTheaterDate(theaterId, checkDate);
		List<httpShowtime> returnList = new ArrayList<httpShowtime>(showtimes.size());
		for(IShowtime showtime:showtimes){
			availTickets = showtimeRepository.getAvailableTickets(showtime.getShowtimeId());
			returnList.add(new httpShowtime(showtime, availTickets));
		}
		
		return returnList;
	}
}
