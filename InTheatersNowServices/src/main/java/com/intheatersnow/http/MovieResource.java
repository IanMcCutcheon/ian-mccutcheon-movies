/**
 * Support for the /movies endpoint.  This allows the API user to:
 * - query for all movies
 * - add a movie
 * - update a movie (not supported yet)
 */
package com.intheatersnow.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intheatersnow.Entity.GenreName;
import com.intheatersnow.Entity.IMovie;
import com.intheatersnow.Entity.Movie;
import com.intheatersnow.Entity.MovieRating;
import com.intheatersnow.httpEntity.httpMovie;
import com.intheatersnow.repository.MovieRepository;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

import io.swagger.annotations.Api;

@Api(value="/movies")
@Path("/movies")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class MovieResource {
	@Autowired
	private MovieRepository movieRepository;
		
	@GET
	@Path("/")
	@Wrapped(element="movies")
	public List<httpMovie>getAllMovies() {
		List<IMovie> movies = movieRepository.getAllMovies();
		List<httpMovie> returnList = new ArrayList<httpMovie>(movies.size());
		for(IMovie movie:movies){
			returnList.add(new httpMovie(movie));
		}
		
		return returnList;
	}
	
	@GET
	@Path("{id}")
	public httpMovie getMovieById(@PathParam("id")long id) {
		System.out.println("Got request for movie by id: " + id);
		
		IMovie movie = movieRepository.getMovieById(id);
		if (movie == null) {
			throw new INTNException(ErrorCode.NOT_FOUND, "Movie does not exist");
		}
		
		return new httpMovie(movie);
	}
	
	@GET
	@Path("/query")
	@Wrapped(element="movies")
	public List<httpMovie> getMoviesByTitle(@QueryParam("title") String title) {
		System.out.println("Getting movie by title");
		List<IMovie> movies = movieRepository.getMovieByTitle(title);	
		List<httpMovie> returnList = new ArrayList<httpMovie>(movies.size());
		for(IMovie movie:movies){
			returnList.add(new httpMovie(movie));
		}
		
		return returnList;
	}
	
	
	@POST
	@Path("/")
	public Response addMovie(httpMovie newMovie){
		Movie movieToCreate = convert(newMovie);
		long movieId = movieRepository.addMovie(movieToCreate);
		newMovie.id = movieId;
		return Response.status(Status.CREATED).header("Location", "/movies/"+movieId).entity(newMovie).build();
	}
	
    @PUT
    @Path("/")
	public Response updateMovie(httpMovie updateMovie) {
    	System.out.println("Got request to update a movie " + updateMovie.title);
    	
		boolean updated = movieRepository.update(convert(updateMovie));
		if (updated) {
			return Response.status(Status.OK).entity(updateMovie).build();
		}
		
		throw new INTNException(ErrorCode.MISSING_DATA, "Movie was missing some fields");
	}	

	private Movie convert(httpMovie httpMovie) {
		Movie movie = new Movie();
		movie.setId(httpMovie.id);
		movie.setTitle(httpMovie.title);
		movie.setRating(new MovieRating(httpMovie.rating));
		movie.setRunningTime(httpMovie.runningTime);
		movie.setRatingExplanation(httpMovie.ratingExplaination);
		movie.setReleaseDate(httpMovie.releaseDate);
		movie.setSynopsis(httpMovie.synopsis);
		
		if (httpMovie.genres != null) {
			for (String genre:httpMovie.genres) {
				movie.addGenre(new GenreName(genre));
			}
		}

		return movie;
	}	
}
