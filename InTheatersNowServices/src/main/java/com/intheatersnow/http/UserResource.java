/**
 * Support for the /users endpoint.  This allows the API user to:
 * - get all users
 * - query for a user
 * - add a user
 * - update a user (not supported yet)
 * - purchase tickets for a user
 */package com.intheatersnow.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intheatersnow.Entity.IUser;
import com.intheatersnow.Entity.User;
import com.intheatersnow.httpEntity.httpPurchase;
import com.intheatersnow.httpEntity.httpPurchaseRequest;
import com.intheatersnow.httpEntity.httpTicketValidation;
import com.intheatersnow.httpEntity.httpUserRequest;
import com.intheatersnow.httpEntity.httpUserResponse;
import com.intheatersnow.repository.PurchaseRepository;
import com.intheatersnow.repository.UserRepository;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

@Path("/users")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class UserResource {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PurchaseRepository purchaseRepository;
	
	
	@GET
	@Path("/")
	@Wrapped(element="users")
    public List<httpUserResponse> getAllUsers() {
		System.out.println("Got user request");
		List<IUser> users = userRepository.getAllUsers();
		List<httpUserResponse> returnList = new ArrayList<httpUserResponse>(users.size());
		for(IUser user:users){
			returnList.add(new httpUserResponse(user));
		}
		
		return returnList;
	}
    
    @POST
    @Path("/")
	public Response addUser(httpUserRequest newUser){
    	httpUserResponse userResponse;
    	
		IUser userToCreate = this.convert(newUser);
		long userId = userRepository.addUser(userToCreate);
		
		userToCreate = userRepository.getUserById(userId);
		userResponse = new httpUserResponse(userToCreate);
		return Response.status(Status.CREATED).header("Location", "/users/"+userId).entity(userResponse).build();
	}	
    
    @PUT
    @Path("/")
	public Response updateUser(httpUserRequest updateUser){
		IUser user = userRepository.getUserById(updateUser.id);
		if (user == null) {
			throw new INTNException(ErrorCode.NOT_FOUND, "Account does not exist");
		}
		
		boolean updated = userRepository.update(convert(updateUser));
		if (updated) {
			return Response.status(Status.OK).entity(new httpUserResponse(user)).build();
		}
	 	
		throw new INTNException(ErrorCode.MISSING_DATA, "User was missing some fields");
	}	

    @GET
	@Path("/{id}")
	public httpUserResponse getUserById(@PathParam("id")long id) {
		IUser user = userRepository.getUserById(id);
		if (user == null) {
			throw new INTNException(ErrorCode.NOT_FOUND, "Id does not exist");
		}
		
		return new httpUserResponse(user);
	}

    @GET
    @Path("/query")
	public httpUserResponse getUserByAccount(@QueryParam("account")String account) {
		IUser user = userRepository.getUserByAccount(account);
		if (user == null) {
			throw new INTNException(ErrorCode.NOT_FOUND, "Account does not exist");
		}
		
		return new httpUserResponse(user);
	}
    
    @POST
 	@Path("/purchases")
 	public Response purchaseTickets(httpPurchaseRequest purchaseRequest) {
 		List<String> barcodes = userRepository.purchaseTickets(purchaseRequest.accountNumber, purchaseRequest.showtimeId, purchaseRequest.tickets);

 		httpPurchase purchase = new httpPurchase();
 		purchase.accountNumber = purchaseRequest.accountNumber;
 		purchase.showtimeId = purchaseRequest.showtimeId;
 		purchase.barcodes = barcodes;
 		
 		return Response.status(Status.CREATED).header("Location", "/purchases/1").entity(purchase).build();
 	}
    
    /**
     * Allows a theater to validate that a customer's ticket is valid
     * @param id, ticket's barcode
     * @return true if the ticket is valid, false if it is not valid or has been used
     */
    @PUT
 	@Path("/purchases/validateTicket/")
 	public Response validateTicket(httpTicketValidation ticket) {
 		ticket.valid = purchaseRepository.validateTicket(ticket.barcode);
 		return Response.status(Status.OK).entity(ticket).build();
 	}
    
    private IUser convert(httpUserRequest httpUser) {
		IUser user = new User();
		user.setName(httpUser.name);
		user.setAccount(httpUser.account);
		user.setStreet1(httpUser.street1);
		user.setStreet2(httpUser.street2);
		user.setCity(httpUser.city);
		user.setState(httpUser.state);
		user.setZipCode(httpUser.zipCode);
		user.setDateJoined(httpUser.whenJoined);
		user.setCreditCardExpiration(httpUser.creditCardExpiration);
		user.setCreditCardNumber(httpUser.creditCardNumber);
		user.setCreditCardSecurityCode(httpUser.creditCardSecurityCode);

		return user;
	}	
}
