/**
 * Support for the /theaters endpoint.  This allows the API user to:
 * - get all theaters and movie screens
 * - query for a theater (not supported yet)
 * - add a theater (not supported yet)
 * - add a movie screen (not supported yet)
 * - update a theater (not supported yet)
 */
package com.intheatersnow.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intheatersnow.Entity.ITheater;
import com.intheatersnow.Entity.Theater;
import com.intheatersnow.httpEntity.httpTheater;
import com.intheatersnow.repository.ShowtimeRepository;
import com.intheatersnow.repository.TheaterRespository;
import com.intheatersnow.repositoryException.ErrorCode;
import com.intheatersnow.repositoryException.INTNException;

import io.swagger.annotations.Api;

@Api(value="/theaters")
@Path("/theaters")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class TheaterResource {
	@Autowired
	private TheaterRespository theaterRepository;
		
	@Autowired
	private ShowtimeRepository showtimeRepository;
	@GET
	
	@Wrapped(element="theaters")
	public List<httpTheater>getAllTheaters() {
		List<ITheater> theaters = theaterRepository.getAllTheaters();
		List<httpTheater> returnList = new ArrayList<httpTheater>(theaters.size());
		for(ITheater theater:theaters){
			returnList.add(new httpTheater(theater));
		}
		
		return returnList;
	}
	
	@GET
	@Path("{id}")
	public httpTheater getTheaterById(@PathParam("id")long id) {
		System.out.println("Got request for theater by id: " + id);
		
		ITheater theater = theaterRepository.getTheaterById(id);
		if (theater == null) {
			throw new INTNException(ErrorCode.NOT_FOUND, "Theater does not exist");
		}
		
		return new httpTheater(theater);
	}
	
	@GET
	@Wrapped(element="theaters")
	public List<httpTheater> getTheaterByZipCode(@QueryParam("zipCode") int zipCode) {
		System.out.println("Getting theater by zip code");
		List<ITheater> theaters = theaterRepository.getTheaterByZipCode(zipCode);	
		List<httpTheater> returnList = new ArrayList<httpTheater>(theaters.size());
		for(ITheater theater:theaters){
			returnList.add(new httpTheater(theater));
		}
		
		return returnList;
	}

	@GET
	@Wrapped(element="theaters")
	public List<httpTheater> getTheaterByCityState(@DefaultValue("") @QueryParam("city") String city, 
                                                   @DefaultValue("") @QueryParam("state") String state) {
		System.out.println("Getting theater by city/state");
		List<ITheater> theaters = theaterRepository.getTheaterByCityAndState(city, state);	
		List<httpTheater> returnList = new ArrayList<httpTheater>(theaters.size());
		for(ITheater theater:theaters){
			returnList.add(new httpTheater(theater));
		}
		
		return returnList;
	}
	
	@GET
	@Path("/movie/{id}")
	@Wrapped(element="theaters")
	public List<httpTheater> getTheatersShowingMovie(@PathParam("id") long id) { 
		System.out.println("Getting theaters showing movie");
		List<ITheater> theaters = showtimeRepository.getTheatersShowingMovie(id);	
		List<httpTheater> returnList = new ArrayList<httpTheater>(theaters.size());
		for(ITheater theater:theaters){
			returnList.add(new httpTheater(theater));
		}
		
		return returnList;
	}
	
	@POST
	public Response addTheater(httpTheater newTheater){
		Theater theaterToCreate = convert(newTheater);
		long theaterId = theaterRepository.addTheater(theaterToCreate);
		newTheater.id = theaterId;
		return Response.status(Status.CREATED).header("Location", "/theaters/"+theaterId).entity(newTheater).build();
	}	
	
	private Theater convert(httpTheater httpTheater) {
		Theater theater = new Theater();
		theater.setName(httpTheater.name);
		theater.setPhoneNumber(httpTheater.phoneNumber);
		theater.setStreet1(httpTheater.street1);
		theater.setStreet2(httpTheater.street2);
		theater.setCity(httpTheater.city);
		theater.setState(httpTheater.state);
		theater.setZipCode(httpTheater.zipCode);
		return theater;
	}
}
