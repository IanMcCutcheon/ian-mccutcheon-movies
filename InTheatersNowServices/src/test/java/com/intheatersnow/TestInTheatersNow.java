package com.intheatersnow;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.intheatersnow.Entity.GenreName;
import com.intheatersnow.Entity.IMovie;
import com.intheatersnow.Entity.MovieRating;
import com.intheatersnow.Entity.IMovieScreen;
import com.intheatersnow.Entity.ScreenType;
import com.intheatersnow.Entity.SeatType;
import com.intheatersnow.Entity.Showtime;
import com.intheatersnow.Entity.IShowtime;
import com.intheatersnow.Entity.ITheater;
import com.intheatersnow.Entity.ITicket;
import com.intheatersnow.Entity.IUser;
import com.intheatersnow.Entity.Movie;
import com.intheatersnow.Entity.Theater;
import com.intheatersnow.Entity.Ticket;
import com.intheatersnow.Entity.MovieScreen;
import com.intheatersnow.Entity.TicketType;
import com.intheatersnow.Entity.User;
//import com.intheatersnow.config.InTheatersNowConfig;
import com.intheatersnow.repository.MovieRepository;
import com.intheatersnow.repository.PurchaseRepository;
import com.intheatersnow.repository.ShowtimeRepository;
import com.intheatersnow.repository.TheaterRespository;
import com.intheatersnow.repository.UserRepository;
import com.intheatersnow.repositoryException.INTNException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.UUID;

//@RunWith(SpringJUnit4ClassRunner.class)
//@Rollback(false)
@ContextConfiguration(locations = {"classpath:spring-context.xml"})
//@ContextConfiguration(classes=InTheatersNowConfig.class) 
public class TestInTheatersNow extends AbstractTransactionalJUnit4SpringContextTests {
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Autowired
	private MovieRepository movieRepository;
	
	@Autowired
	private TheaterRespository theaterRepository;

	@Autowired
	private ShowtimeRepository showtimeRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PurchaseRepository purchaseRepository;

	@Test
	public void GetAllMoviesFromDatabase() {
		List<IMovie> movies = movieRepository.getAllMovies();
		for (IMovie movie : movies) {
			System.out.println("Title: " + movie.getTitle());
		}
		
		long number = movieRepository.getNumberMovies();
		System.out.println("Number of movies in db: " + number);
		
	}
	
	
	
	@Test
	public void MovieFromDatabase() {
		Calendar release;
		
		IMovie movie = movieRepository.getMovieById(1);
		System.out.println("Movie name: " + movie.getTitle());
		release = movie.getReleaseDate();
		System.out.println("Release date: " + release.get(Calendar.YEAR) + " - " + release.get(Calendar.MONTH) + 1 + " - " + release.get(Calendar.DAY_OF_MONTH));
		System.out.println("Rating: " + movie.getRating());
		Set<GenreName> genres = movie.getGenres();
		for (GenreName genre : genres) {
			System.out.println("Genre: " + genre.getGenre());
		}
		
		movie = movieRepository.getMovieById(2);
		System.out.println("Movie name: " + movie.getTitle());
		release = movie.getReleaseDate();
		System.out.println("Release date: " + release.get(Calendar.YEAR) + " - " + release.get(Calendar.MONTH) + 1 + " - " + release.get(Calendar.DAY_OF_MONTH));
		System.out.println("Rating: " + movie.getRating());
		genres = movie.getGenres();
		for (GenreName genre : genres) {
			System.out.println("Genre: " + genre.getGenre());
		}
		
	}
	
	@Test
	public void TheatersFromDatabase() {
		ITheater theater = theaterRepository.getTheaterById(1);
		System.out.println("Theater name: " + theater.getName());
		List<IMovieScreen> screens = theater.getScreens();
		for (IMovieScreen screen : screens) {
			System.out.println("Name: " + screen.getName());
			System.out.println("Number Seats: " + screen.getNumberSeats());
			System.out.println("Screen Type: " + screen.getScreenType());
			System.out.println("Seat Type: " + screen.getSeatType());
		}
	}
		
	
	
	
	
	
	/**
	 * Get a single movie that does exist
	 */
	@Test
	public void testGetSingleMovieByTitle(){
		List<IMovie>movies = movieRepository.getMovieByTitle("Star Wars: Episode IV - A New Hope");
		Assert.assertEquals(1, movies.size());
		
		for (IMovie movie : movies) {
			Assert.assertEquals(movie.getTitle(), "Star Wars: Episode IV - A New Hope");
		}
	}
	
	/**
	 * Get movies that match a partial name
	 */
	@Test
	public void testGetMoviesByPartialTitle(){
		List<IMovie>movies = movieRepository.getMovieByTitle("Star");
		Assert.assertEquals(2, movies.size());

		for (IMovie movie : movies) {
			Assert.assertTrue(movie.getTitle().contains("Star"));
		}
	}
	
	/**
	 * Try to get a movie that is not in the database
	 */
	@Test
	public void testGetMovieThatDoesNotExistByTitle(){
		List<IMovie>movies = movieRepository.getMovieByTitle("A Star is Born");
		Assert.assertEquals(0, movies.size());
	}

	/**
	 * Get multiple movies that have the same name
	 */
	@Test
	public void testGetMultipleMoviesByTitle(){
		List<IMovie>movies = movieRepository.getMovieByTitle("A Christmas Carol");
		Assert.assertEquals(2, movies.size());
		
		for (IMovie movie : movies) {
			System.out.println("Got movie: " + movie.getTitle());
			Assert.assertEquals(movie.getTitle(), "A Christmas Carol");
		}
	}
	
	/**
	 * Test add movie.  Note this also tests getAllMovies since it uses
	 * this to verify the movie was added.
	 */
	@Test
	public void testAddMovie(){
		IMovie movie = new Movie();
		MovieRating rating = new MovieRating();
		Calendar release = Calendar.getInstance();
		GenreName genre;
		
		long number = movieRepository.getNumberMovies();
		Assert.assertEquals(8, number);
		
		movie.setTitle("Papa Hemingway in Cuba");
		
		rating.setRating("R");
		movie.setRating(rating);
		
		release.set(2016, 3, 29);
		movie.setReleaseDate(release);
		movie.setRatingExplanation("Language, sexuality, some violance and nudity."); 
		movie.setRunningTime(109);
		movie.setSynopsis("In 1959, a young journalist ventures to Havana, Cuba to a meet his idol, "
				+ "the legendary Ernest Hemingway who helped him find his literary voice, while the "
				+ "Cuban Revolution comes to a boil around them.");
		
		genre = new GenreName();
		genre.setGenre("Biography");
		movie.addGenre(genre);

		genre = new GenreName();
		genre.setGenre("Drama");
		movie.addGenre(genre);
		
		movieRepository.addMovie(movie);

		number = movieRepository.getNumberMovies();
		Assert.assertEquals(9, number);
	}
	
	
	/**
	 * Try to find a theater by zip code but use a zip code that does not match
	 * any theaters in the database.
	 */
	@Test
	public void testGetNoMatchZipCodeTheater(){
		List<ITheater>theaters = theaterRepository.getTheaterByZipCode(11111);
		Assert.assertEquals(0, theaters.size());
	}
	
	/**
	 * Find theaters by zip code
	 */
	@Test
	public void testGetTheaterByZipCode() {
		List<ITheater>theaters = theaterRepository.getTheaterByZipCode(94568);
		Assert.assertEquals(1, theaters.size());
		Assert.assertEquals(theaters.get(0).getName(), "Regal Hacienda Crossing Stadium 20 & IMAX");
	}
	
	/**
	 * Find theaters by name
	 */
	@Test
	public void testGetTheaterByName() {
		List<ITheater>theaters = theaterRepository.getTheaterByName("AMC");
		Assert.assertEquals(1, theaters.size());
		
		for (ITheater theater : theaters) {
			System.out.println("Got theater: " + theater.getName());
			Assert.assertTrue(theater.getName().contains("AMC"));
		}
	}
	
	/**
	 * Try to find a theater by name but use a name for a theater that does not
	 * match any theaters in the database. 
	 */
	@Test
	public void testGetNoMatchTheaterByName(){
		List<ITheater>theaters = theaterRepository.getTheaterByName("XYZ");
		Assert.assertEquals(0, theaters.size());
	}

	/**
	 * Find theaters by city and state
	 */
	@Test
	public void testGetTheaterByCityState(){
		List<ITheater>theaters = theaterRepository.getTheaterByCityAndState("Dublin", "CA");
		Assert.assertEquals(1, theaters.size());
		
		for (ITheater theater : theaters) {
			Assert.assertTrue(theater.getName().contains("Regal"));
		}
	}
	
	/**
	 * Add a new theater
	 */
	@Test
	public void testAddTheater(){
		List<ITheater>theaters = theaterRepository.getTheaterByCityAndState("San Francisco", "CA");
		Assert.assertEquals(1, theaters.size());
		
		ITheater newTheater = new Theater();
		newTheater.setName("AMC Van Ness 14");
		newTheater.setStreet1("1000 Van Ness Avenue");
		newTheater.setCity("San Francisco");
		newTheater.setState("CA");
		
		IMovieScreen newScreen = new MovieScreen();
		newScreen.setName("Theater 1");
		newScreen.setScreenType(new ScreenType("Conventional"));
		newScreen.setSeatType(new SeatType("Stadium"));
		newScreen.setNumberSeats(150);
		newScreen.setDevice(true);
		newTheater.addScreen(newScreen);
		
		newScreen = new MovieScreen();
		newScreen.setName("Theater 2");
		newScreen.setScreenType(new ScreenType("Real3D"));
		newScreen.setSeatType(new SeatType("Stadium"));
		newScreen.setNumberSeats(110);
		newScreen.setDevice(false);
		newTheater.addScreen(newScreen);
		
		theaterRepository.addTheater(newTheater);
		
		theaters = theaterRepository.getTheaterByCityAndState("San Francisco", "CA");
		Assert.assertEquals(2, theaters.size());
	}
	
	/**
	 * Add a new screen to an existing theater
	 */
	@Test
	public void testAddScreen(){
		List<ITheater>theaters = theaterRepository.getTheaterByCityAndState("San Francisco", "CA");
		Assert.assertEquals(1, theaters.size());
		
		ITheater theater = theaters.get(0);
		List<IMovieScreen> screens = theater.getScreens();
		Assert.assertEquals(1, screens.size());
		
		IMovieScreen newScreen = new MovieScreen();
		newScreen.setName("Theater 2");
		newScreen.setScreenType(new ScreenType("Conventional"));
		newScreen.setSeatType(new SeatType("Stadium"));
		newScreen.setNumberSeats(150);
		newScreen.setDevice(true);
		theaterRepository.addScreen(theater.getId(), newScreen);
		
		ITheater checkTheater = theaterRepository.getTheaterById(theater.getId());
		List<IMovieScreen> checkScreens = checkTheater.getScreens();
		Assert.assertEquals(2, checkScreens.size());
		IMovieScreen checkScreen = checkScreens.get(1);
		
		Assert.assertEquals(newScreen.getName(), checkScreen.getName());
		Assert.assertEquals(newScreen.getScreenType(), checkScreen.getScreenType());
		Assert.assertEquals(newScreen.getSeatType(), checkScreen.getSeatType());
		Assert.assertEquals(newScreen.getNumberSeats(), checkScreen.getNumberSeats());
		Assert.assertEquals(newScreen.getAccessibilityDeviceAvailable(), checkScreen.getAccessibilityDeviceAvailable());
	}
	
	/**
	 * Try to find a theater by city and state but use a city/state for a theater that does not
	 * match any theaters in the database. 
	 */
	@Test
	public void testGetNoMatchTheaterByCityState(){
		List<ITheater>theaters = theaterRepository.getTheaterByCityAndState("San Ramon", "CA");
		Assert.assertEquals(0, theaters.size());
	}

	/**
	 * Get all movies that are showing in a specified theater
	 */
	@Test
	public void testGetMoviesShowingInTheater() {
		String title;
		boolean foundStarWars = false;
		boolean foundRevenant = false;
		boolean foundStarTrek = false;
		
		System.out.println("Get movies showing in theater");
		
		Calendar queryDate = Calendar.getInstance();
		queryDate.set(2016, Calendar.JULY, 4);
		List<ITheater>theaters = theaterRepository.getTheaterByName("Regal Hacienda Crossing Stadium 20 & IMAX");
		
		System.out.println("Got theater: " + theaters.size() + "id: " + theaters.get(0).getId());
		
		List<IMovie> movies = showtimeRepository.getMoviesByTheaterAndDate(theaters.get(0).getId(), queryDate);
		
		System.out.println("Got movies: " + movies.size());
		Assert.assertEquals(3, movies.size());
		
		for (IMovie movie : movies) {
			title = movie.getTitle();
			if (!foundStarWars && title.matches("Star Wars: Episode IV - A New Hope")) {
					foundStarWars = true;
			}
			if (!foundStarTrek && title.matches("Star Trek Into Darkness")) {
				foundStarTrek = true;
			}
			if (!foundRevenant && title.matches("The Revenant")) {
				foundRevenant = true;
			}
		}		
		
		Assert.assertTrue(foundStarWars);
		Assert.assertTrue(foundRevenant);
		Assert.assertTrue(foundStarTrek);
	}

	
	/**
	 * Find the only theater showing a particular movie  
	 */
	@Test
	public void testGetOneTheaterShowingMovie(){
		List<IMovie>movies = movieRepository.getMovieByTitle("Star Wars: Episode IV - A New Hope");
		List<ITheater>theaters = showtimeRepository.getTheatersShowingMovie(movies.get(0).getId());
		Assert.assertEquals(1, theaters.size());
		
		for (ITheater theater : theaters) {
			Assert.assertTrue(theater.getName().contains("Regal"));
		}
	}
	
	/**
	 * Find all the theaters showing a particular movie  
	 */
	@Test
	public void testGetAllTheatersShowingMovie(){
		List<IMovie>movies = movieRepository.getMovieByTitle("Toy Story");
		List<ITheater>theaters = showtimeRepository.getTheatersShowingMovie(movies.get(0).getId());
		Assert.assertEquals(2, theaters.size());
		
		Assert.assertTrue(theaters.get(0).getName().contains("Regal Hacienda Crossing Stadium 20 & IMAX"));
		Assert.assertTrue(theaters.get(1).getName().contains("AMC Metreon 16"));
	}

	
	/**
	 * Get a movie that is in the database but not assigned to any theater.  Verify that an 
	 * empty theater list is returned.
	 */
	@Test
	public void testGetMovieNotShowing(){
		List<IMovie>movies = movieRepository.getMovieByTitle("A Christmas Carol");
		
		List<ITheater>theaters = showtimeRepository.getTheatersShowingMovie(movies.get(0).getId());
		Assert.assertEquals(0, theaters.size());
	}
	
	/**
	 * Get a list of the movies playing at a particular theater on a given date. 
	 */
	@Test
	public void testGetShowtimesForMovieAtTheaterOnDate(){
		System.out.println("Test get showtimes for movie");
		Calendar checkDate = Calendar.getInstance();
		checkDate.clear();
		checkDate.set(2016, Calendar.JULY, 4);
		List<IShowtime> showtimes = showtimeRepository.getShowtimesByTheaterMovieDate(1, 1, checkDate);
		System.out.println("Num showtimes: " + showtimes.size());
		Assert.assertEquals(2, showtimes.size());
		
		System.out.println("Showtime: " + showtimes.get(0).getShowtime());
		System.out.println("Showtime: " + showtimes.get(1).getShowtime());

		checkDate.set(2016, Calendar.JULY, 4, 10, 30, 00);
		Assert.assertEquals(showtimes.get(0).getShowtime(), checkDate);
		checkDate.set(2016, Calendar.JULY, 4, 12, 00, 00);
		Assert.assertEquals(showtimes.get(1).getShowtime(), checkDate);
	}
	
	/**
	 * Check a date for which there are no showtime. 
	 */
	@Test
	public void testGetNoShowtimesForMovieAtTheaterOnDate(){
		System.out.println("Test get no showtimes for movie");
		Calendar checkDate = Calendar.getInstance();
		checkDate.clear();
		checkDate.set(2016, Calendar.APRIL, 30);
		List<IShowtime> showtimes = showtimeRepository.getShowtimesByTheaterMovieDate(1, 1, checkDate);
		System.out.println("Num showtimes: " + showtimes.size());
		Assert.assertEquals(0, showtimes.size());
	}

	
	/**
	 * Get all showtimes at a theater on a particular date. 
	 */
	@Test
	public void testGetAllShowtimesAtTheaterOnDate(){
		System.out.println("Test get all showtimes for theater");
		Calendar checkDate = Calendar.getInstance();
		checkDate.clear();
		checkDate.set(2016, Calendar.JULY, 4);
		List<IShowtime> showtimes = showtimeRepository.getShowtimesByTheaterDate(1, checkDate);
		System.out.println("Num showtimes: " + showtimes.size());
		Assert.assertEquals(4, showtimes.size());
	}
	
	/**
	 * Add a new showtime. 
	 */
	@Test
	public void testAddShowtime(){
		Calendar showing = Calendar.getInstance();
		
		IShowtime showtime = new Showtime();
		showtime.setMovieId(12);
		showtime.setTheaterId(2);
		showtime.setScreenId(5);
		
		showing.set(2016,  Calendar.JULY, 4, 18, 15, 00);
		showtime.setShowtime(showing);
		
		ITicket ticket = new Ticket();
		ticket.setPrice(13.00f);
		ticket.setTicketType(new TicketType("Adult"));
		showtime.addTicket(ticket);
		
		ticket = new Ticket();
		ticket.setPrice(8.00f);
		ticket.setTicketType(new TicketType("Child"));
		showtime.addTicket(ticket);

		ticket = new Ticket();
		ticket.setPrice(8.00f);
		ticket.setTicketType(new TicketType("Senior"));
		showtime.addTicket(ticket);
		
		long id = showtimeRepository.addShowtime(showtime);
		Assert.assertTrue(id > 0);
		
		Showtime checkShowtime = showtimeRepository.getShowtimeById(id);
		Assert.assertNotNull(checkShowtime);
		
		Assert.assertEquals(showtime.getShowtimeId(), checkShowtime.getShowtimeId());
		Assert.assertEquals(showtime.getMovieId(), checkShowtime.getMovieId());
		Assert.assertEquals(showtime.getTheaterId(), checkShowtime.getTheaterId());
		Assert.assertEquals(showtime.getScreenId(), checkShowtime.getScreenId());
		Assert.assertEquals(showtime.getShowtime(), checkShowtime.getShowtime());
		Assert.assertEquals(showtime.getTickets().size(), checkShowtime.getTickets().size());
		
		List<ITicket> origTickets = showtime.getTickets();
		List<ITicket> checkTickets = checkShowtime.getTickets();
		for (int i = 0; i < showtime.getTickets().size(); ++i) {
			Assert.assertEquals(origTickets.get(i).getPrice(), checkTickets.get(i).getPrice(), 0.1);
			Assert.assertEquals(origTickets.get(i).getTicketType(), checkTickets.get(i).getTicketType());
		}
	}
	
	/**
	 * Attempt to update a non-existent showtime. 
	 */
	@Test
	public void testUpdateInvalidShowtime(){
		Showtime showtime = showtimeRepository.getShowtimeById(1);
		Assert.assertEquals(showtime.getShowtimeId(), 1);
		showtime.setShowtimeId(9999);
		Assert.assertFalse(showtimeRepository.update(showtime));
	}

	/**
	 * Get a movie that is in the database and update it.  Verify the updates are recorded 
	 * into the database.
	 */
	@Test
	public void testUpdateMovie(){
		MovieRating rating;
		
		List<IMovie>movies = movieRepository.getMovieByTitle("The Matrix");
		Assert.assertEquals(1, movies.size());
		
		IMovie movie = movies.get(0);
		Assert.assertTrue(movie.getTitle().compareTo("The Matrix") == 0);
		Assert.assertEquals(movie.getRating(), "R");
		
		rating = new MovieRating("PG-13");
		movie.setRating(rating);
		Assert.assertTrue(movieRepository.update(movie));
		
		movies = movieRepository.getMovieByTitle("The Matrix");
		Assert.assertEquals(1, movies.size());
		movie = movies.get(0);
		Assert.assertTrue(movie.getTitle().compareTo("The Matrix") == 0);
		Assert.assertEquals(movie.getRating(), "PG-13");

		/* Put the original rating back */
		rating = new MovieRating("R");
		movie.setRating(rating);
		Assert.assertTrue(movieRepository.update(movie));

		movies = movieRepository.getMovieByTitle("The Matrix");
		Assert.assertEquals(1, movies.size());
		movie = movies.get(0);
		Assert.assertTrue(movie.getTitle().compareTo("The Matrix") == 0);
		Assert.assertEquals(movie.getRating(), "R");
	}

	/**
	 * Attempt to update a movie that does not exist in the database.
	 */
	@Test
	public void testUpdateNonExistingMovie(){
		List<IMovie>movies = movieRepository.getMovieByTitle("Superman");
		Assert.assertEquals(0, movies.size());

		IMovie movie = new Movie();
		MovieRating rating;
		Calendar release;
		GenreName genre;
		
		System.out.println("Trying to update new movie");
		movie.setTitle("Superman");
		
		rating = new MovieRating("PG");
		movie.setRating(rating);
		
		release = Calendar.getInstance();
		release.set(1978, 12, 15);
		movie.setReleaseDate(release);
		movie.setRatingExplanation("Peril, some mild sensualty and language"); 
		movie.setRunningTime(143);
		movie.setSynopsis("An alien orphan is sent from his dying plant to Earth, where he grows up to "
				+ "become his adoptive home's first and greatest superhero.");
		
		genre = new GenreName("Adventure");
		movie.addGenre(genre);
		genre = new GenreName("Action");
		movie.addGenre(genre);
		genre = new GenreName("Drama");
		movie.addGenre(genre);

		Assert.assertFalse(movieRepository.update(movie));
		
		movies = movieRepository.getMovieByTitle("Superman");
		Assert.assertEquals(0, movies.size());
	}

	/**
	 * Verify can purchase 1 ticket 
	 */
	@Test
	public void testPurchaseATicket(){
		Calendar purchaseDate = Calendar.getInstance();
		purchaseDate.set(2016, Calendar.JULY, 4, 0, 0, 0);
		List<IShowtime> showtimes = showtimeRepository.getShowtimesByTheaterMovieDate(1, 1, purchaseDate);
		Assert.assertEquals(2, showtimes.size());
		
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 200); 
		
    	Assert.assertTrue(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 1)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 199); 
	}
	
	/**
	 * Verify can't purchase more tickets than are available 
	 */
	@Test
	public void testPurchaseTooManyTickets(){
		Calendar purchaseDate = Calendar.getInstance();
		purchaseDate.set(2016, Calendar.JULY, 4, 0, 0, 0);
		
		List<IShowtime> showtimes = showtimeRepository.getShowtimesByTheaterMovieDate(1, 1, purchaseDate);
		Assert.assertEquals(2, showtimes.size());
		
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 200); 
		
		Assert.assertFalse(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 201)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 200); 
	}

	/**
	 * Verify can purchase last ticket 
	 */
	@Test
	public void testPurchaseLastTicket(){
		Calendar purchaseDate = Calendar.getInstance();
		purchaseDate.set(2016, Calendar.JULY, 4, 0, 0, 0);
		
		List<IShowtime> showtimes = showtimeRepository.getShowtimesByTheaterMovieDate(1, 1, purchaseDate);
		Assert.assertEquals(2, showtimes.size());
		
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 200); 
		
		Assert.assertTrue(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 50)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 150); 

		Assert.assertTrue(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 49)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 101);
		
		Assert.assertTrue(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 48)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 53);

		Assert.assertTrue(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 52)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 1); 

		/* Buy the last ticket */
		Assert.assertTrue(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 1)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 0); 

		/* Try to buy  one more than the last */
		Assert.assertFalse(showtimeRepository.purchaseTickets(showtimes.get(0).getShowtimeId(), 1)); 
		Assert.assertEquals(showtimeRepository.getAvailableTickets(showtimes.get(0).getShowtimeId()), 0); 
	}
	
	/**
	 * Verify can't purchase tickets for an invalid showtime 
	 */
	@Test
	public void testPurchaseTicketsForInvalidShowtime(){
		try {
			Assert.assertEquals(showtimeRepository.getAvailableTickets(1000L), 0); 
		
			Assert.assertFalse(showtimeRepository.purchaseTickets(1000L, 1)); 
			Assert.assertEquals(showtimeRepository.getAvailableTickets(1000L), 0);
		}
		catch (Exception ex) {
			System.out.println("Exception " + ex.getMessage());
		}
	}
	
	/**
	 * Verify can't purchase tickets for a showtime that has past 
	 */
	@Test
	public void testPurchaseTicketsForExpiredShowtime(){
		try {
			Assert.assertEquals(showtimeRepository.getAvailableTickets(7L), 168); 
		
			Assert.assertFalse(showtimeRepository.purchaseTickets(7L, 1)); 
			Assert.assertEquals(showtimeRepository.getAvailableTickets(7L), 168);
		}
		catch (Exception ex) {
			System.out.println("Exception " + ex.getMessage());
		}
	}

	
	/**
	 * Add a user whose account already exists 
	 */
	@Test
	public void testAddUserExistingAccount() {
		Calendar joinDate;

		long numberUsers = userRepository.getNumberUsers();
		Assert.assertEquals(3, numberUsers);
		
		IUser user = new User();
		user.setAccount("123456789");
		joinDate = Calendar.getInstance();
		joinDate.set(2016,  05, 16);
		user.setDateJoined(joinDate);
		user.setName("Alex James");
		
		thrown.expect(INTNException.class);
		thrown.expectMessage("Account number already used");
		long userId = userRepository.addUser(user);
		Assert.assertEquals(userId, -1);

		numberUsers = userRepository.getNumberUsers();
		Assert.assertEquals(3, numberUsers);
	}
	
	/**
	 * Add a user whose account does not already exists 
	 */
	@Test
	public void testAddUserNewAccount() {
		Calendar joinDate;
		
		long numberUsers = userRepository.getNumberUsers();
		Assert.assertEquals(3, numberUsers);
		
		IUser user = new User();
		user.setAccount("987654321");
		
		joinDate = Calendar.getInstance();
		joinDate.set(2016,  05, 16);
		user.setDateJoined(joinDate);
		user.setName("Alex James");
		userRepository.addUser(user);

		numberUsers = userRepository.getNumberUsers();
		Assert.assertEquals(4, numberUsers);
	}
	
	/**
	 * Add a user whose account does not already exists 
	 */
	@Test
	public void testAddUserInvalidAccount() {
		Calendar joinDate;
		
		System.out.println("Adding user with invalid account number");
		long numberUsers = userRepository.getNumberUsers();
		Assert.assertEquals(3, numberUsers);
		
		IUser user = new User();
		user.setAccount("abc56usdt");
		
		joinDate = Calendar.getInstance();
		joinDate.set(2016,  05, 16);
		user.setDateJoined(joinDate);
		user.setName("Alex James");

		thrown.expect(INTNException.class);
		thrown.expectMessage("Account number must be nine digits long");
		userRepository.addUser(user);

		numberUsers = userRepository.getNumberUsers();
		Assert.assertEquals(3, numberUsers);
	}
	
	
	
	/**
	 * Get user with valid account 
	 */
	@Test
	public void testGetValidUserByAccount() {
		IUser user = userRepository.getUserByAccount("123456789");
		Assert.assertTrue(user.getAccount().contentEquals("123456789"));
		Assert.assertTrue(user.getName().contentEquals("Joe Brown"));
	}
	
	/**
	 * Try to get a user that does not exist 
	 */
	@Test
	public void testGetInvalidUserByAccount() {
		IUser user = userRepository.getUserByAccount("128967");
		Assert.assertNull("Got an account when did not expect one", user);
	}

	/**
	 * Purchase tickets for a valid user 
	 */
	@Test
	public void testPurchaseTicketsForUser() {
		TicketType ticket;
		List<TicketType>tickets = new ArrayList<TicketType>();
		
		System.out.println("Purchase Tickets for User");
		
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Child");
		tickets.add(ticket);
		
		try {
			List<String> barcodes = userRepository.purchaseTickets("123456789", 1, tickets); 
			Assert.assertEquals(3, barcodes.size());
		
			for (int i = 0; i < barcodes.size(); ++i) {
				System.out.println("Barcode: " + barcodes.get(i));
			}
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}
	
	/**
	 * Purchase tickets for an invalid user 
	 */
	@Test
	public void testPurchaseTicketsForInvalidUser() {
		TicketType ticket;
		List<TicketType>tickets = new ArrayList<TicketType>();
		
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Child");
		tickets.add(ticket);
		
		System.out.println("Purchase Tickets for invalid user");
		try {
			List<String> barcodes = userRepository.purchaseTickets("4558934", 1, tickets); 
			Assert.assertEquals(0, barcodes.size());
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}
	
	/**
	 * Purchase tickets for valid user with expired credit card 
	 */
	@Test
	public void testPurchaseTicketsWithExpiredCreditCard() {
		TicketType ticket;
		List<TicketType>tickets = new ArrayList<TicketType>();
		
		System.out.println("Purchase Tickets with expired credit card");
		
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Child");
		tickets.add(ticket);

		try {
			List<String> barcodes = userRepository.purchaseTickets("587445221", 1, tickets); 
			Assert.assertEquals(0, barcodes.size());
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}
	
	/**
	 * Purchase tickets for valid user with invalid credit card 
	 */
	@Test
	public void testPurchaseTicketsWithInvalidCreditCard() {
		TicketType ticket;
		List<TicketType>tickets = new ArrayList<TicketType>();
		
		System.out.println("Purchase Tickets With Invalid Credit Card");
		
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Adult");
		tickets.add(ticket);
		ticket = new TicketType("Child");
		tickets.add(ticket);

		try {
			List<String> barcodes = userRepository.purchaseTickets("655390871", 1, tickets); 
			Assert.assertEquals(0, barcodes.size());
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	/**
	 * Verify invalid ticket is marked as invalid 
	 */
	@Test
	public void testValidateInvalidTicket() {
		System.out.println("Attempting to validate an invalid ticket");
		Assert.assertFalse(purchaseRepository.validateTicket(UUID.randomUUID().toString()));
	}

	/**
	 * Verify valid ticket is marked as valid 
	 */
	@Test
	public void testValidateValidTicket() {
		System.out.println("Attempting to validate a valid tickets");
		
		List<String>tickets = purchaseRepository.purchaseTickets(1, 4);
		Assert.assertEquals(4, tickets.size());
		
		for (int i = 0; i < tickets.size(); ++i) {
			Assert.assertTrue(purchaseRepository.validateTicket(tickets.get(i)));
		}			
	}
	
	/**
	 * Verify used ticket is marked as invalid 
	 */
	@Test
	public void testInvalidateUsedTicket() {
		System.out.println("Attempt to revalidate a used, valid ticket");
		
		List<String>tickets = purchaseRepository.purchaseTickets(1, 8);
		System.out.println("Number tickets: " + tickets.size());
		Assert.assertEquals(8, tickets.size());
		
		for (int i = 0; i < tickets.size(); ++i) {
			Assert.assertTrue(purchaseRepository.validateTicket(tickets.get(i)));
			Assert.assertFalse(purchaseRepository.validateTicket(tickets.get(i)));
		}			
	}
}

