## In Theaters Now

A project for Enterprise Applications with Java course.

This simulates the backend of a website like Fandango.com.  It provides support for querying 
movies, theaters and showtimes and allows a registered used to purchase tickets.  Now supports MySQL using Hibernate so values can be persisted.

Load the program using Eclipse and run the JUnit test TestInTheatersNow.java to verify 
the functionality.

