package com.intheatersnow.http;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestTheaterResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "InTheatersNowServices/rest/theaters";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		System.out.println("Creating target");
		target = client.target(HTTP_HOST).path(URI_PATH);
	}
	
	/**
	 * Get all theaters in the database
	 */
	@Test
	public void testGetAllTheaters(){	
		System.out.println("Getting all theaters");
		Response response =	target.request().accept(MediaType.APPLICATION_XML).get();

		List<httpTheater> searchResponse = response.readEntity(new GenericType<List<httpTheater>>(){});
		int status = response.getStatus();
		System.out.println("Number theaters: " + searchResponse.size());
		System.out.println("HTTP Status: " + status);
		for (httpTheater theater:searchResponse) {
			System.out.println(theater.toString());
			for (httpMovieScreen screen:theater.screens) {
				System.out.println(screen.toString());
			}
		}
		
		Assert.assertEquals(200, status);
		Assert.assertEquals(2, searchResponse.size());

		response.close();
	}
	
	/**
	 * Get all theaters in the database
	 */
	@Test
	public void testTheatersShowingMovie(){	
		System.out.println("Getting theaters showing movie");
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/movie/" + 11) 
				.request().accept(MediaType.APPLICATION_JSON).get();

		List<httpTheater> searchResponse = response.readEntity(new GenericType<List<httpTheater>>(){});
		int status = response.getStatus();
		System.out.println("Number theaters: " + searchResponse.size());
		System.out.println("HTTP Status: " + status);
		for (httpTheater theater:searchResponse) {
			System.out.println(theater.toString());
		}
		
		Assert.assertEquals(200, status);
	}
}
