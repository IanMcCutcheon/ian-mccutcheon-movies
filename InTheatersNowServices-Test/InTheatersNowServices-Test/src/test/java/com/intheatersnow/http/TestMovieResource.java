package com.intheatersnow.http;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.intheatersnow.http.httpError;

public class TestMovieResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "InTheatersNowServices/rest/movies";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		System.out.println("Creating target");
		target = client.target(HTTP_HOST).path(URI_PATH);
	}

	/**
	 * Get all movies in the database
	 */
	@Test
	public void testGetAllMovies(){	
		System.out.println("Getting all movies");
		Response response =	target.request().accept(MediaType.APPLICATION_XML).get();

		List<httpMovie> searchResponse = response.readEntity(new GenericType<List<httpMovie>>(){});
		int status = response.getStatus();
		System.out.println("Number movies: " + searchResponse.size());
		System.out.println("HTTP Status: " + status);
		System.out.println("Title: " + searchResponse.get(0).title);
		
		Assert.assertEquals(200, status);
		Assert.assertEquals(10, searchResponse.size());
	}
	
	/**
	 * Test adding a new movie to the database
	 */
	@Test
	public void testAddMovie(){
		httpMovie newMovie = new httpMovie();
		newMovie.title = "Alice In Wonderland";
		newMovie.runningTime = 108;
		newMovie.rating = "PG";
		newMovie.ratingExplaination = "Fantasy action/violence involving scary images"
				+ "and situations, and for a smoking catepillar";
		newMovie.releaseDate = Calendar.getInstance();
		newMovie.releaseDate.set(2010, Calendar.MARCH, 5);
		newMovie.synopsis = "Nineteen-year-old Alice returns to the magical world from her childhood adventure, "
				+ "where she reunites with her old friends and learns of her true destiny: "
				+ "to end the Red Queen's reign of terror.";
		newMovie.genres = new ArrayList<String>();
		newMovie.genres.add("Adventure");
		newMovie.genres.add("Family");
		newMovie.genres.add("Fantasy");
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(newMovie, MediaType.APPLICATION_JSON));
		
		httpMovie createResponse = response.readEntity(httpMovie.class);
		System.out.println("Movie Id: " + createResponse.id);
		Assert.assertEquals(201, response.getStatus());
		Assert.assertEquals(createResponse.title, newMovie.title);
		Assert.assertEquals(createResponse.rating, newMovie.rating);
		Assert.assertNotNull(createResponse.id);
	}
	
	
	/**
	 * Test adding a new movie to the database
	 */
	@Test
	public void testUpdateMovie(){
		System.out.println("Test updating movie");
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/11") 
				.request().accept(MediaType.APPLICATION_JSON).get();
		
		int status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(200, status);
		
		httpMovie movie = response.readEntity(httpMovie.class);
		System.out.println("Movie Id: " + movie.id);
		if (movie.genres != null) {
			System.out.println("Genre: " + movie.genres.get(0));
		}			
			
		Assert.assertEquals(movie.title, "Toy Story");
		Assert.assertEquals(movie.rating, "G");
		Assert.assertEquals(11, movie.id);
		
		movie.rating = "PG-13";
		movie.title = "Too many Toy Story";
		response = target.request().accept(MediaType.APPLICATION_JSON).put(Entity.entity(movie, MediaType.APPLICATION_JSON));
		status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		movie = response.readEntity(httpMovie.class);
		if (movie.genres != null) {
			System.out.println("Genre: " + movie.genres.get(0));
		}			
		Assert.assertEquals(200, status);
		Assert.assertEquals(11, movie.id);
		Assert.assertEquals(movie.title, "Too many Toy Story");
		Assert.assertEquals(movie.rating, "PG-13");

		/* Now put the original settings back */
		movie.rating = "G";
		movie.title = "Toy Story";
		response = target.request().accept(MediaType.APPLICATION_JSON).put(Entity.entity(movie, MediaType.APPLICATION_JSON));
		status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		movie = response.readEntity(httpMovie.class);
		Assert.assertEquals(200, status);
		Assert.assertEquals(movie.title, "Toy Story");
		Assert.assertEquals(movie.rating, "G");

		/* Get the movie again to confirm has been changed back */
		response = client.target(HTTP_HOST).path(URI_PATH + "/11") 
				.request().accept(MediaType.APPLICATION_JSON).get();
		movie = response.readEntity(httpMovie.class);
		System.out.println("Movie Id: " + movie.id);
		status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		
		Assert.assertEquals(200, status);
		Assert.assertEquals(11, movie.id);
		Assert.assertEquals(movie.title, "Toy Story");
		Assert.assertEquals(movie.rating, "G");
	}

	/**
	 * Verify can't add a movie with an invalid rating to the database
	 */
	@Test
	public void testAddMovieInvalidRating(){
		httpMovie newMovie = new httpMovie();
		newMovie.title = "Alice In Wonderland";
		newMovie.runningTime = 108;
		newMovie.rating = "PGA";
		newMovie.ratingExplaination = "Fantasy action/violence involving scary images"
				+ "and situations, and for a smoking catepillar";
		newMovie.releaseDate = Calendar.getInstance();
		newMovie.releaseDate.set(2010, Calendar.MARCH, 5);
		newMovie.synopsis = "Nineteen-year-old Alice returns to the magical world from her childhood adventure, "
				+ "where she reunites with her old friends and learns of her true destiny: "
				+ "to end the Red Queen's reign of terror.";
		newMovie.genres = new ArrayList<String>();
		newMovie.genres.add("Adventure");
		newMovie.genres.add("Family");
		newMovie.genres.add("Fantasy");
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(newMovie, MediaType.APPLICATION_JSON));
		
		int status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(409, status);
		
		httpError error = response.readEntity(httpError.class);
		System.out.println("Error: " + error.toString());		
		Assert.assertEquals(error.message, "Unknown rating PGA");
	}
	
	/**
	 * Verify can't add a movie with an invalid title length to the database
	 */
	@Test
	public void testAddMovieInvalidTitleLength(){
		httpMovie newMovie = new httpMovie();
		newMovie.title = new String(new char[260]).replace('\0', '*');
		newMovie.runningTime = 108;
		newMovie.rating = "PG";
		newMovie.ratingExplaination = "Fantasy action/violence involving scary images"
				+ "and situations, and for a smoking catepillar";
		newMovie.releaseDate = Calendar.getInstance();
		newMovie.releaseDate.set(2010, Calendar.MARCH, 5);
		newMovie.synopsis = "Nineteen-year-old Alice returns to the magical world from her childhood adventure, "
				+ "where she reunites with her old friends and learns of her true destiny: "
				+ "to end the Red Queen's reign of terror.";
		newMovie.genres = new ArrayList<String>();
		newMovie.genres.add("Adventure");
		newMovie.genres.add("Family");
		newMovie.genres.add("Fantasy");
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(newMovie, MediaType.APPLICATION_JSON));
		
		int status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(409, status);
		
		httpError error = response.readEntity(httpError.class);
		System.out.println("Error: " + error.toString());		
		Assert.assertEquals(error.message, "Title invalid length");
	}

	/**
	 * Verify can't add a movie with an invalid synopsis length to the database
	 */
	@Test
	public void testAddMovieInvalidSynopsisLength(){
		httpMovie newMovie = new httpMovie();
		newMovie.title = "Alice In Wonderland";
		newMovie.runningTime = 108;
		newMovie.rating = "PG";
		newMovie.ratingExplaination = "Fantasy action/violence involving scary images"
				+ "and situations, and for a smoking catepillar";
		newMovie.releaseDate = Calendar.getInstance();
		newMovie.releaseDate.set(2010, Calendar.MARCH, 5);
		newMovie.synopsis = new String(new char[513]).replace('\0', '*');
		newMovie.genres = new ArrayList<String>();
		newMovie.genres.add("Adventure");
		newMovie.genres.add("Family");
		newMovie.genres.add("Fantasy");
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(newMovie, MediaType.APPLICATION_JSON));
		
		int status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(409, status);
		
		httpError error = response.readEntity(httpError.class);
		System.out.println("Error: " + error.toString());		
		Assert.assertEquals(error.message, "Synopsis too long");
	}
	
	/**
	 * Verify can't add a movie with an invalid genre to the database
	 */
	@Test
	public void testAddMovieInvalidGenre(){
		httpMovie newMovie = new httpMovie();
		newMovie.title = "Alice In Wonderland";
		newMovie.runningTime = 108;
		newMovie.rating = "PG";
		newMovie.ratingExplaination = "Fantasy action/violence involving scary images"
				+ "and situations, and for a smoking catepillar";
		newMovie.releaseDate = Calendar.getInstance();
		newMovie.releaseDate.set(2010, Calendar.MARCH, 5);
		newMovie.synopsis = "Nineteen-year-old Alice returns to the magical world from her childhood adventure, "
				+ "where she reunites with her old friends and learns of her true destiny: "
				+ "to end the Red Queen's reign of terror.";
		newMovie.genres = new ArrayList<String>();
		newMovie.genres.add("Adventre");
		newMovie.genres.add("Family");
		newMovie.genres.add("Fantasy");
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(newMovie, MediaType.APPLICATION_JSON));
		
		int status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(409, status);
		
		httpError error = response.readEntity(httpError.class);
		System.out.println("Error: " + error.toString());		
		Assert.assertEquals(error.message, "Unknown genre Adventre");
	}

	/**
	 * Try to get the list of movies by title 
	 */
	@Test
	public void testGetMovieByTitle(){	
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/query")
				.queryParam("title", "Star")
				.request().accept(MediaType.APPLICATION_JSON).get();
		
		int status = response.getStatus();
		List<httpMovie> movies = response.readEntity(new GenericType<List<httpMovie>>(){});
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(200, status);
		
		for (httpMovie movie:movies) {
			System.out.println("Title: " + movie.title);
		}
	}
	
	/** 
	 * Try to get a movie using an invalid id
	 */
	@Test
	public void testGetMovieByInvalidId(){	
		System.out.println("Getting movie with invalid id");
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/9999") 
				.request().accept(MediaType.APPLICATION_JSON).get();
		
		int status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(404, status);
		
		httpError error = response.readEntity(httpError.class);
		System.out.println("Error: " + error.toString());
	}
	
	/**
	 * Verify can get a movie with a valid id
	 */
	@Test
	public void testGetMovieByValidId(){	
		System.out.println("Getting movie with Valid id");
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/11") 
				.request().accept(MediaType.APPLICATION_JSON).get();
		
		int status = response.getStatus();
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(200, status);
		
		httpMovie movie = response.readEntity(httpMovie.class);
		System.out.println("Movie Id: " + movie.id);
		Assert.assertEquals(movie.title, "Toy Story");
		Assert.assertEquals(movie.rating, "G");
		Assert.assertEquals(11, movie.id);
	}
}
