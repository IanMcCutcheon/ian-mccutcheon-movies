package com.intheatersnow.http;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.intheatersnow.util.TicketType;

public class TestUserResource {
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "InTheatersNowServices/rest/users";
	
	private Client client = ClientBuilder.newClient();
	private WebTarget target;
	
	@Before
	public void init(){
		System.out.println("Creating target");
		target = client.target(HTTP_HOST).path(URI_PATH);
	}

	/**
	 * Get all users in the database
	 */
	@Test
	public void testGetAllUsers(){	
		System.out.println("Get all users");
		
		Response response =	target.request().accept(MediaType.APPLICATION_XML).get();

		List<httpUser> searchResponse = response.readEntity(new GenericType<List<httpUser>>(){});
		int status = response.getStatus();
		System.out.println("Number users=" + searchResponse.size());
		System.out.println("HTTP Status=" + status);
		System.out.println("Name=" + searchResponse.get(0).name);
		
		Assert.assertEquals(200, status);
		Assert.assertEquals(3, searchResponse.size());
	}

	/**
	 * Get a user by their account number
	 */
	@Test
	public void testGetUserByAccount(){	
		System.out.println("Get user by valid account number");
		
		try {
			Response response = client.target(HTTP_HOST).path(URI_PATH + "/query")
					.queryParam("account", "123456789")
					.request().accept(MediaType.APPLICATION_JSON).get();
		
			int status = response.getStatus();
			httpUser user = response.readEntity(httpUser.class);
			System.out.println("HTTP Status=" + status);
			System.out.println("Name=" + user.name);
			Assert.assertEquals(200, status);
			Assert.assertEquals("Joe Brown", user.name);
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}
	
	/** 
	 * Try to get a user with an invalid account number
	 */
	@Test
	public void testGetUserByInvalidAccount(){	
		System.out.println("Get user by invalid account number");
		
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/query")
				.queryParam("account", "abcd")
				.request().accept(MediaType.APPLICATION_JSON).get();
		try {
			int status = response.getStatus();
			httpError error = response.readEntity(httpError.class);
			System.out.println("HTTP Status=" + status);
			Assert.assertEquals(404, status);
			System.out.println("Error: " + error.toString());
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}	

	/**
	 * Try to purchase tickets with an invalid account number
	 */
	@Test
	public void testPurchaseTicketsInvalidAccount(){	
		System.out.println("Attempting to purchase tickets with invalid account number");
		httpPurchaseRequest purchaseRequest = new httpPurchaseRequest();
		
		purchaseRequest.accountNumber = "abc";
		purchaseRequest.showtimeId = 1;
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/purchases")
					.request().accept(MediaType.APPLICATION_JSON)
					.post(Entity.entity(purchaseRequest, MediaType.APPLICATION_JSON));
		try {
			int status = response.getStatus();
			httpError error = response.readEntity(httpError.class);
			System.out.println("HTTP Status=" + status);
			//Assert.assertEquals(404, status);
			System.out.println("Error: " + error.toString());
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}
	
	/**
	 * Verify can purchase tickets with a valid account number
	 */
	@Test
	public void testPurchaseTicketsValidAccount(){	
		System.out.println("Attempting to purchase tickets");
		httpPurchaseRequest purchaseRequest = new httpPurchaseRequest();
		
		purchaseRequest.accountNumber = "123456789";
		purchaseRequest.showtimeId = 1;
		List<TicketType>tickets = new ArrayList<TicketType>();
		tickets.add(new TicketType("Adult"));
		tickets.add(new TicketType("Adult"));
		tickets.add(new TicketType("Child"));
		purchaseRequest.tickets = tickets;
		
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/purchases")
					.request().accept(MediaType.APPLICATION_JSON)
					.post(Entity.entity(purchaseRequest, MediaType.APPLICATION_JSON));
		try {
			int status = response.getStatus();
			httpPurchase purchase = response.readEntity(httpPurchase.class);
			System.out.println("HTTP Status=" + status);
			Assert.assertEquals(201, status);
			for (String barcode:purchase.barcodes) {
				System.out.println("Barcode: " + barcode);
			}
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}	

	/**
	 * Verify can purchase a ticket and it is valid until used
	 */
	@Test
	public void testValidateTicket(){
		httpPurchase purchase = null;
		int status;
		
		System.out.println("Attempting to validate ticket");
		httpPurchaseRequest purchaseRequest = new httpPurchaseRequest();
		
		purchaseRequest.accountNumber = "123456789";
		purchaseRequest.showtimeId = 1;
		List<TicketType>tickets = new ArrayList<TicketType>();
		tickets.add(new TicketType("Adult"));
		tickets.add(new TicketType("Adult"));
		tickets.add(new TicketType("Child"));
		purchaseRequest.tickets = tickets;
		
		Response response = client.target(HTTP_HOST).path(URI_PATH + "/purchases")
					.request().accept(MediaType.APPLICATION_JSON)
					.post(Entity.entity(purchaseRequest, MediaType.APPLICATION_JSON));
		try {
			status = response.getStatus();
			purchase = response.readEntity(httpPurchase.class);
			System.out.println("HTTP Status=" + status);
			Assert.assertEquals(201, status);
			for (String barcode:purchase.barcodes) {
				System.out.println("Barcode: " + barcode);
			}
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}

		/* Now check that the tickets are valid */
		httpTicketValidation validTicket;
		
		try {
			for (String barcode:purchase.barcodes) {
				validTicket  = new httpTicketValidation();
				validTicket.barcode = barcode;
				
				response = client.target(HTTP_HOST).path(URI_PATH + "/purchases/validateTicket/")
						.request().accept(MediaType.APPLICATION_JSON)
						.put(Entity.entity(validTicket, MediaType.APPLICATION_JSON));
			
				status = response.getStatus();
				validTicket = response.readEntity(httpTicketValidation.class);
				System.out.println(validTicket.toString());
				Assert.assertEquals(200, status);
				Assert.assertTrue(validTicket.valid);
			}
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
		
		/* Now the tickets should not be valid */
		try {
			for (String barcode:purchase.barcodes) {
				validTicket  = new httpTicketValidation();
				validTicket.barcode = barcode;

				response = client.target(HTTP_HOST).path(URI_PATH + "/purchases/validateTicket/")
						.request().accept(MediaType.APPLICATION_JSON)
						.put(Entity.entity(validTicket, MediaType.APPLICATION_JSON));
			
				status = response.getStatus();
				validTicket = response.readEntity(httpTicketValidation.class);
				System.out.println(validTicket.toString());
				Assert.assertEquals(200, status);
				Assert.assertFalse(validTicket.valid);
			}
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}	
	
	/**
	 * Verify can't validate a ticket with a unsupported (fake) barcode
	 */
	@Test
	public void testValidateFakeTicket(){
		httpTicketValidation validTicket;
		httpError error;
		int status;
		
		System.out.println("Attempting to validate fake ticket");
		try {
			validTicket  = new httpTicketValidation();
			validTicket.barcode = UUID.randomUUID().toString();

			Response response = client.target(HTTP_HOST).path(URI_PATH + "/purchases/validateTicket/")
						.request().accept(MediaType.APPLICATION_JSON)
						.put(Entity.entity(validTicket, MediaType.APPLICATION_JSON));
			
			status = response.getStatus();
			Assert.assertEquals(404, status);
			error = response.readEntity(httpError.class);
			System.out.println("Error message: " + error.message);
		}
		catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	/**
	 * Try to get the user but don't provide a query parameter
	 */
	@Test
	public void testGetUsersNoParamsXml(){
		try {
			System.out.println("Making a request with no parameters");
		
			Response response = client.target(HTTP_HOST).path(URI_PATH + "/query")
					.queryParam("account", "")
					.request()
					.accept(MediaType.APPLICATION_XML)
					.get();
		
			httpError error = response.readEntity(httpError.class);
		
			Assert.assertEquals(409, response.getStatus());
			Assert.assertEquals(409, error.status);
			Assert.assertEquals("MISSING_DATA", error.code);
			Assert.assertEquals("no account number provided", error.message);
			Assert.assertEquals("", error.debug);
		}
		
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
/*
	private void verifyMissing(Response response) {
		HttpError error = response.readEntity(HttpError.class);
		Assert.assertEquals(409, response.getStatus());
		Assert.assertEquals(409, error.status);
		Assert.assertEquals("MISSING_DATA", error.code);
		Assert.assertEquals("no search parameter provided", error.message);
		Assert.assertEquals("", error.debug);		
	}
	
	@Test
	public void testCreateUsersNoParamsXml(){					
		Response response =	target.request().accept(MediaType.APPLICATION_XML).post(Entity.entity("<user/>", MediaType.APPLICATION_XML));
		
		verifyInvalid(response);
	}
	
	@Test
	public void testCreateUsersNoParamsEntityXml(){					
		HttpUser userToSend = new HttpUser();		
		Response response =	target.request().accept(MediaType.APPLICATION_XML).post(Entity.entity(userToSend, MediaType.APPLICATION_XML));
		
		verifyInvalid(response);
	}
	
	@Test
	public void testCreateUsersNoParamsJson(){					
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity("{user:{}}", MediaType.APPLICATION_JSON));
		
		verifyInvalid(response);
	}
	
	@Test
	public void testCreateUsersNoParamsEntityJson(){					
		HttpUser userToSend = new HttpUser();		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(userToSend, MediaType.APPLICATION_JSON));
		
		verifyInvalid(response);
	}

	private void verifyInvalid(Response response) {
		HttpError error = response.readEntity(HttpError.class);
		Assert.assertEquals(409, response.getStatus());
		Assert.assertEquals(409, error.status);
		Assert.assertEquals("INVALID_FIELD", error.code);
		Assert.assertEquals("pin is required", error.message);
		Assert.assertEquals("", error.debug);		
	}
	
	@Test
	public void testCreateAndGetUser(){					
		HttpUser userToSend = new HttpUser();
		userToSend.firstName="foo"+new Random().nextInt(99999);
		userToSend.lastName="bar"+new Random().nextInt(99999);;
		userToSend.pin="12345";
		
		Response response =	target.request().accept(MediaType.APPLICATION_JSON).post(Entity.entity(userToSend, MediaType.APPLICATION_JSON));
		
		HttpUser createResponse = response.readEntity(HttpUser.class);
		//System.err.println(createResponse);
		Assert.assertEquals(201, response.getStatus());
		Assert.assertEquals(createResponse.firstName, userToSend.firstName);
		Assert.assertEquals(createResponse.lastName, userToSend.lastName);
		Assert.assertNotNull(createResponse.id);
		Assert.assertNull(createResponse.pin);
		
		//search for just created user		
		Response search = target.queryParam("firstName", userToSend.firstName).queryParam("lastName", userToSend.lastName).request().accept(MediaType.APPLICATION_JSON).get();
		List<HttpUser> searchResponse = search.readEntity(new GenericType<List<HttpUser>>(){});
		Assert.assertEquals(searchResponse.get(0), createResponse);		
	}
	*/
}
