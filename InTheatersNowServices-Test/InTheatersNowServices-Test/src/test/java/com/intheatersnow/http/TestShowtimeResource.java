package com.intheatersnow.http;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestShowtimeResource {
	
	private static final String HTTP_HOST = "http://localhost:8080";
	private static final String URI_PATH = "InTheatersNowServices/rest/showtimes";
	
	private Client client = ClientBuilder.newClient();
	
	@Before
	public void init(){
	}
	
	public httpTheater getTheater(long id) {
		Response response = client.target(HTTP_HOST).path("InTheatersNowServices/rest/theaters/" + id) 
				.request().accept(MediaType.APPLICATION_JSON).get();
		
		return response.readEntity(httpTheater.class);
	}
	
	public httpMovie getMovie(long id) {
		Response response = client.target(HTTP_HOST).path("InTheatersNowServices/rest/movies/" + id) 
				.request().accept(MediaType.APPLICATION_JSON).get();
		
		return response.readEntity(httpMovie.class);
	}

	/**
	 * Get all showtimes at a theater on a particular date
	 */
	@Test
	public void testGetShowtimesByTheaterDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		System.out.println("Getting showtimes for theater on date");
		
		Response response = client.target(HTTP_HOST).path(URI_PATH)
				.queryParam("theater", "1")
				.queryParam("date", "2016-07-04")
				.request().accept(MediaType.APPLICATION_JSON).get();
		
		int status = response.getStatus();
		List<httpShowtime> showtimes = response.readEntity(new GenericType<List<httpShowtime>>(){});
		System.out.println("HTTP Status: " + status);
		Assert.assertEquals(200, status);
		
		List<httpTicket>tickets;
		
		try {
			for (httpShowtime showtime:showtimes) {
				System.out.println("Showtime Id: " + showtime.showtimeId);
//				System.out.println("Theater Id: " + showtime.theaterId);
			
				httpTheater hTheater = getTheater(showtime.theaterId);
				System.out.println(hTheater.toString());
			
//				System.out.println("Screen Id: " + showtime.screenId);
				System.out.println(hTheater.screens.get((int)showtime.screenId).toString());
			
//				System.out.println("Movie Id: " + showtime.movieId);
				httpMovie hMovie = getMovie(showtime.movieId);
				System.out.println(hMovie.toString());

				System.out.println("Showing: " + sdf.format(showtime.showing.getTime()));
				System.out.println("Tickets Available: " + showtime.ticketsAvailable);
				tickets = showtime.tickets;
				if (tickets == null) {
					System.out.println("Ticket pricing not available");
				}
				else {
					for (httpTicket ticket:tickets) {
						System.out.println(ticket.toString());
					}
				}
			
				System.out.println("\n\n----------------------------------------------------\n\n");
			}
		}
		catch(Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}
}
