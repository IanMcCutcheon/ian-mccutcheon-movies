/**
 * Defines the format for an HTTP error message.
 */
package com.intheatersnow.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "error")
public class httpError {
	@XmlElement
	public int status;
	
	@XmlElement
	public String code;
	
	@XmlElement
	public String message;
	
	@XmlElement
	public String debug;

	@Override
	public String toString() {
		return "HttpError [status=" + status + ", code=" + code + ", message="
				+ message + ", debug=" + debug + "]";
	}
	
}
