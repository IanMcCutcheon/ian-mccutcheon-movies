package com.intheatersnow.http;


public class httpTicket {
	public float price;
	
	public String type;
	
	
	@Override
	public String toString() {
		return "httpTicket [type: " + type + ", price $" + String.format("%.2f",  price) + "]";
	}
}
