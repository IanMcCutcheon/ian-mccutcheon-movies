/**
 * Defines the format for the movie as sent over HTTP
 */
package com.intheatersnow.http;

import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "movie")
public class httpMovie {
	@XmlElement
	public long id;
	@XmlElement		
	public String title;
	@XmlElement		
	public String synopsis;
	@XmlElement		
	public Calendar releaseDate;
	@XmlElement
	public String ratingExplaination;
	@XmlElement		
	public long runningTime;
	@XmlElement	
	public String rating;
	@XmlElement
	public List<String>genres;
	
	@Override
	public String toString() {
		return "httpMovie [id: " + id + ", title: " + title + ", synopsis " + synopsis +
				", running time: " + runningTime + ", rating: " + rating + "]";
	}
}
