/**
 * Defines the format for a purchase request as sent over HTTP
 */

package com.intheatersnow.http;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.intheatersnow.util.TicketType;

@XmlRootElement(name = "purchaseRequest")
public class httpPurchaseRequest {
	@XmlElement
	public String accountNumber;
		
	@XmlElement
	public long showtimeId;

	@XmlElement
	public List<TicketType> tickets;
}
