package com.intheatersnow.http;

import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "showtime")
public class httpShowtime {
	@XmlElement
	public long showtimeId;
	
	@XmlElement
	public long theaterId;
	
	@XmlElement
	public long movieId;
	
	@XmlElement
	public long screenId;

	@XmlElement
	public Calendar showing;
	
	@XmlElement
	public List<httpTicket> tickets;
	
	@XmlElement
	public boolean ticketsAvailable;
}
