/**
 * Defines the format of the User as sent over HTTP
 */
package com.intheatersnow.http;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
public class httpUser {
	@XmlElement
	public long id;
	
	@XmlElement
	public String account;

	@XmlElement
	public String name;
	
	@XmlElement
	public String street1;
	
	@XmlElement
	public String street2;
	
	@XmlElement
	public String city;
	
	@XmlElement
	public String state;
	
	@XmlElement
	public int zipCode;
	
	@XmlElement
	public Calendar whenJoined;
	
	// Don't report the credit card information but 
	// do indicate if it is valid or not.
	@XmlElement
	public boolean creditCardValid;
}
