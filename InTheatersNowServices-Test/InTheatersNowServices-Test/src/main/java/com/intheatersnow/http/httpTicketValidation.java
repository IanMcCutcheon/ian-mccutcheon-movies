package com.intheatersnow.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ticketValidation")
public class httpTicketValidation {
	@XmlElement
	public String barcode;
	
	@XmlElement
	public boolean valid;
		
	@XmlElement
	public String notValidReason;
		
	@Override
	public String toString() {
		return "httpTicketValidation [barcode: " + barcode + ", valid: " + valid + "]";
	}
}
