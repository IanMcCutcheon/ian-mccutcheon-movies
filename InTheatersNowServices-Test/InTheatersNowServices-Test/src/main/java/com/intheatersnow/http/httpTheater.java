/**
 * Defines the structure of a theater as sent over HTTP
 */
package com.intheatersnow.http;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "theater")
public class httpTheater {
	@XmlElement
	public long id;
	@XmlElement
	public String name;
	@XmlElement
	public String phoneNumber;
	@XmlElement
	public String street1;
	@XmlElement
	public String street2;
	@XmlElement
	public String city;
	@XmlElement
	public String state;
	@XmlElement
	private int zipCode;
	@XmlElement
	public List<httpMovieScreen> screens;
	
	@Override
	public String toString() {
		return "httpTheater [id: " + id + ", name: " + name
				+ ", phone number: " + phoneNumber + ", street1: " + street1
				+ ", street 2: " + street2 + ", city: " 
				+ city + ", state: " + state + ", zip code: " + zipCode + "]";
	}
}
