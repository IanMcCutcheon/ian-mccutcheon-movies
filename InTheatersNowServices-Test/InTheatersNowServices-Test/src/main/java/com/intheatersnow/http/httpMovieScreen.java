/**
 * Defines the structure of a movie screen as sent over HTTP
 */
package com.intheatersnow.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "movieScreen")
public class httpMovieScreen {
	@XmlElement
	public long id;
	
	@XmlElement
	public String name;
	
	@XmlElement
	public String seatType;
	
	@XmlElement
	public int numberSeats;
	
	@XmlElement
	public String screenType;
	
	@XmlElement
	public boolean accessibilityDevicePresent;
	
	@Override
	public String toString() {
		return "httpMovieScreen [id: " + id + ", name: " + name
				+ ", number seats: " + numberSeats + ", seat type: " + seatType
				+ ", screen type: " + screenType + ", accessibility device present: " 
				+ accessibilityDevicePresent + "]";
	}
}
