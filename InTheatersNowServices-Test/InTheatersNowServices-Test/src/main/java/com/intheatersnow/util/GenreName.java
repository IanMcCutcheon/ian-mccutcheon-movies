/**
 * MovieGenre.java
 * 
 * List of Film Genres, from IMDb (www.imdb.com/genre)
 *
 */

package com.intheatersnow.util;

public class GenreName {
	private String genre;
	
	public GenreName() { 
	}
	
	public GenreName(String genre) { 
		this.genre = genre;
	}
	
	public String getGenre() {
		return this.genre;
	}
	
	public void setGenre(String genre) {
		this.genre  = genre;
	}
}
