/**
 * TicketType.java
 * 
 * List of different types of tickets that can be purchased.
 */

package com.intheatersnow.util;


public class TicketType {
	private String type;

	public TicketType() {
	}
	
	public TicketType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setRating(String type) {
		this.type  = type;
	}
}
